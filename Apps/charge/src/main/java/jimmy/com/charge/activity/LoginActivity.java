package jimmy.com.charge.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.TemplateUtils;
import com.jimmy.common.util.ToastUtils;
import com.uuzuche.lib_zxing.activity.CaptureActivity;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import jimmy.com.charge.R;
import jimmy.com.charge.api.WebApi;
import jimmy.com.charge.config.ApiCode;
import jimmy.com.charge.config.SharedKey;
import jimmy.com.charge.config.WorkerConfig;
import jimmy.com.charge.data.User;
import jimmy.com.charge.data.Worker;
import jimmy.com.charge.fragment.LoginFragment;
import jimmy.com.charge.fragment.MainFragment;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private int SCAN_CODE = 1;

    private Button btnLogin, btnScan, btnLogout, btnScanGun;

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_login);
        btnLogin = searchViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        btnScan = searchViewById(R.id.btnScan);
        btnScan.setOnClickListener(this);
        btnLogout = searchViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);
        btnScanGun = searchViewById(R.id.btnScanGun);
        btnScanGun.setOnClickListener(this);
    }

    @Override
    protected void bindData() {
        super.bindData();
        if (SharedUtils.getBoolean(this, SharedKey.IS_LOGIN)) {
            btnLogin.setVisibility(View.GONE);
            btnScan.setVisibility(View.VISIBLE);
            btnScanGun.setVisibility(View.VISIBLE);
            btnLogout.setVisibility(View.VISIBLE);
            WorkerConfig.getInstance().worker = new Gson().fromJson(SharedUtils.getString(this, SharedKey.WORKER_CONFIG), Worker.class);
        } else {
            btnLogin.setVisibility(View.VISIBLE);
            btnScan.setVisibility(View.GONE);
            btnScanGun.setVisibility(View.GONE);
            btnLogout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                TemplateUtils.startTemplate(this, LoginFragment.class, getString(R.string.login));
                break;
            case R.id.btnScan:
                Intent intent = new Intent(this, CaptureActivity.class);
                startActivityForResult(intent, SCAN_CODE);
                break;
            case R.id.btnLogout:
                SharedUtils.putBoolean(this, SharedKey.IS_LOGIN, false);
                bindData();
                break;
            case R.id.btnScanGun:
                Bundle bundle = new Bundle();
                bundle.putString(CodeUtils.RESULT_STRING, "");
                TemplateUtils.startTemplate(LoginActivity.this, MainFragment.class, getString(R.string.order_list), getString(R.string.scan_again), bundle);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SCAN_CODE) {
            if (data != null) {
                final Bundle bundle = data.getExtras();
                if (bundle != null) {
                    if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                        WebApi.checkUser(bundle.getString(CodeUtils.RESULT_STRING) == null ? "" : bundle.getString(CodeUtils.RESULT_STRING), new OnResponseListener<BaseResponse<User>>() {
                            @Override
                            public void onResponse(BaseResponse<User> response) {
                                if (response.getCode() == ApiCode.SUCCESS && response.getData() != null) {
                                    TemplateUtils.startTemplate(LoginActivity.this, MainFragment.class, getString(R.string.order_list), getString(R.string.scan_again), bundle);
                                } else {
                                    ToastUtils.showShortToast(LoginActivity.this, R.string.user_no_exist);
                                }
                            }
                        });
                    } else {
                        ToastUtils.showToast(this, getString(R.string.scan_failed));
                    }
                }
            }
        }
    }

}
