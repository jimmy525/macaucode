package jimmy.com.charge.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.ToastUtils;
import com.jimmy.common.view.pagerv.PageRecyclerView;
import com.uuzuche.lib_zxing.activity.CaptureActivity;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import java.util.ArrayList;
import java.util.List;

import jimmy.com.charge.R;
import jimmy.com.charge.adapter.GoodsAdapter;
import jimmy.com.charge.api.WebApi;
import jimmy.com.charge.config.ApiCode;
import jimmy.com.charge.data.Goods;
import jimmy.com.charge.data.User;
import jimmy.com.charge.data.binding.GoodsDataBinding;

/**
 * Created by Jimmy on 2017/6/12 0012.
 */
public class MainFragment extends TemplateFragment implements View.OnClickListener {

    private int SCAN_CODE = 1;

    private SwipeRefreshLayout srlRefresh;
    private PageRecyclerView rlGoodsList;
    private TextView tvEmpty;
    private EditText etScanCode;
    private GoodsAdapter goodsAdapter;
    private String user_phone;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    protected void initView() {
        srlRefresh = searchViewById(R.id.srlRefresh);
        rlGoodsList = searchViewById(R.id.rlGoodsList);
        tvEmpty = searchViewById(R.id.tvEmpty);
        etScanCode = searchViewById(R.id.etScanCode);
        searchViewById(R.id.btnPayAllCash).setOnClickListener(this);
        searchViewById(R.id.btnPayAllBalance).setOnClickListener(this);
        initRefresh();
        initGoodsList();
        etScanCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    user_phone = etScanCode.getText().toString();
                    if (!TextUtils.isEmpty(user_phone) && user_phone.length() == 8)
                        bindingData();
                    return true;
                }
                return false;
            }
        });
    }

    private void initGoodsList() {
        List<Goods> goods = new ArrayList<>();
        goodsAdapter = new GoodsAdapter(mContext, goods, R.layout.list_item_goods, etScanCode);
        rlGoodsList.setAdapter(goodsAdapter);
        rlGoodsList.setRefreshLayout(srlRefresh);
        rlGoodsList.setEmptyView(tvEmpty);
    }

    @Override
    protected void initData() {
        user_phone = getArguments().getString(CodeUtils.RESULT_STRING);
        if (!"".equals(user_phone)) {
            bindingData();
            etScanCode.setText(user_phone);
        }
    }

    private void bindingData() {
        rlGoodsList.setDataBinding(new GoodsDataBinding(user_phone));
    }

    private void initRefresh() {
        srlRefresh.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
    }

    @Override
    public void onRightButtonClick(View v) {
        super.onRightButtonClick(v);
        Intent intent = new Intent(mContext, CaptureActivity.class);
        startActivityForResult(intent, SCAN_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SCAN_CODE) {
            if (data != null) {
                final Bundle bundle = data.getExtras();
                if (bundle != null) {
                    if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                        WebApi.checkUser(bundle.getString(CodeUtils.RESULT_STRING) == null ? "" : bundle.getString(CodeUtils.RESULT_STRING), new OnResponseListener<BaseResponse<User>>() {
                            @Override
                            public void onResponse(BaseResponse<User> response) {
                                if (response.getCode() == ApiCode.SUCCESS && response.getData() != null) {
                                    user_phone = bundle.getString(CodeUtils.RESULT_STRING);
                                    etScanCode.setText(user_phone);
                                    bindingData();
                                } else {
                                    ToastUtils.showShortToast(mContext, R.string.user_no_exist);
                                }
                            }
                        });
                    } else {
                        ToastUtils.showToast(mContext, R.string.scan_failed);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPayAllCash:
                goodsAdapter.payAll(1);
                break;
            case R.id.btnPayAllBalance:
                goodsAdapter.payAll(2);
                break;
        }
    }
}
