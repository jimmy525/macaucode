package jimmy.com.charge.config;

/**
 * Created by Administrator on 2017/5/24.
 */

public interface SharedKey {

    String IS_LOGIN = "is.login";
    String USERNAME = "username";
    String PASSWORD = "password";
    String WORKER_CONFIG = "worker.config";

}
