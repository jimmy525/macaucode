package jimmy.com.charge.api;

import com.google.gson.reflect.TypeToken;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.HttpUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jimmy.com.charge.config.ApiUrl;
import jimmy.com.charge.config.WorkerConfig;
import jimmy.com.charge.data.Goods;
import jimmy.com.charge.data.User;
import jimmy.com.charge.data.Worker;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public class WebApi {

    public static void login(String username, String password, OnResponseListener<BaseResponse<Worker>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("admin_name", username);
        params.put("admin_password", password);
        params.put("admin_type", WorkerConfig.PAY);
        HttpUtils.httpPost(ApiUrl.LOGIN, params, listener, new TypeToken<BaseResponse<Worker>>() {
        }.getType());
    }


    public static void checkUser(String user_code, OnResponseListener<BaseResponse<User>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("user_phone", user_code);
        HttpUtils.httpPost(ApiUrl.CHECK_USER, params, listener, new TypeToken<BaseResponse<User>>() {
        }.getType());
    }

    public static void getOrdersList(String phone, OnResponseListener<BaseResponse<List<Goods>>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("user_phone", phone);
        params.put("sign", WorkerConfig.getInstance().getWorkerSign());
        HttpUtils.httpPost(ApiUrl.GOODS_LIST, params, listener, new TypeToken<BaseResponse<List<Goods>>>() {
        }.getType());
    }

    public static void payOrder(String goodsId, int pay, OnResponseListener<BaseResponse<String>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("goods_id", goodsId);
        params.put("pay_way", String.valueOf(pay));
        params.put("sign", WorkerConfig.getInstance().getWorkerSign());
        HttpUtils.httpPost(ApiUrl.PAY_ORDER, params, listener, new TypeToken<BaseResponse<String>>() {
        }.getType());
    }

    public static void payOrders(String ids, int pay, OnResponseListener<BaseResponse<String>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("goods_ids", ids);
        params.put("pay_way", String.valueOf(pay));
        params.put("sign", WorkerConfig.getInstance().getWorkerSign());
        HttpUtils.httpPost(ApiUrl.PAY_ORDERS, params, listener, new TypeToken<BaseResponse<String>>() {
        }.getType());
    }
}
