package jimmy.com.charge.fragment;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.gson.Gson;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.charge.R;
import jimmy.com.charge.api.WebApi;
import jimmy.com.charge.config.ApiCode;
import jimmy.com.charge.config.SharedKey;
import jimmy.com.charge.config.WorkerConfig;
import jimmy.com.charge.data.Worker;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public class LoginFragment extends TemplateFragment implements View.OnClickListener {

    private EditText etUsername, etPassword;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    protected void initView() {
        etUsername = searchViewById(R.id.etUsername);
        etPassword = searchViewById(R.id.etPassword);
        searchViewById(R.id.btnLogin).setOnClickListener(this);
    }

    @Override
    protected void initData() {
        super.initData();
        String username = SharedUtils.getString(mActivity, SharedKey.USERNAME);
        String password = SharedUtils.getString(mActivity, SharedKey.PASSWORD);
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            etUsername.setText(username);
            etPassword.setText(password);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                login();
                break;
        }
    }

    private void login() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            ToastUtils.showShortToast(mActivity, R.string.login_info_is_empty);
            return;
        }
        SharedUtils.putString(mActivity, SharedKey.USERNAME, username);
        SharedUtils.putString(mActivity, SharedKey.PASSWORD, password);
        WebApi.login(username, password, new OnResponseListener<BaseResponse<Worker>>() {
            @Override
            public void onResponse(BaseResponse<Worker> response) {
                if (response.getCode() == ApiCode.SUCCESS) {
                    if (response.getData() != null) {
                        ToastUtils.showShortToast(mActivity, R.string.login_success);
                        WorkerConfig.getInstance().worker = response.getData();
                        SharedUtils.putString(mActivity, SharedKey.WORKER_CONFIG, new Gson().toJson(response.getData()));
                        SharedUtils.putBoolean(mActivity, SharedKey.IS_LOGIN, true);
                        mActivity.finish();
                    }
                } else if (response.getCode() == 2) {
                    ToastUtils.showShortToast(mActivity, R.string.login_failed_no_user);
                } else {
                    ToastUtils.showShortToast(mActivity, R.string.login_failed_password);
                }
            }
        });
    }
}
