package jimmy.com.charge.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.jimmy.common.adapter.SuperAdapter;
import com.jimmy.common.adapter.SuperViewHolder;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.DateUtils;
import com.jimmy.common.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import jimmy.com.charge.R;
import jimmy.com.charge.api.WebApi;
import jimmy.com.charge.config.ApiCode;
import jimmy.com.charge.data.Goods;

/**
 * Created by Administrator on 2017/6/23.
 */

public class GoodsAdapter extends SuperAdapter<Goods> {

    private EditText etScanCode;
    private List<Goods> goods;

    public GoodsAdapter(Context context, List<Goods> items, @LayoutRes int layoutResId, EditText etScanCode) {
        super(context, items, layoutResId);
        this.etScanCode = etScanCode;
        this.goods = new ArrayList<>();
    }

    @Override
    public void onBind(SuperViewHolder holder, int viewType, int position, final Goods item) {
        TextView tvInfo = holder.findViewById(R.id.tvInfo);
        String content = "運單號: " + item.bar_code + "   货架: " + item.area + "\n" +
                "重量: " + item.weight + "kg，" + "尺寸: " + item.length + "cm*" + item.width + "cm*" + item.height + "cm 收費:￥" + item.price + "\n" +
                DateUtils.format(item.update_time * 1000 + 4 * 24 * 3600 * 1000) + "     到期";
        tvInfo.setText(content);
        holder.setOnClickListener(R.id.tvPayCash, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pay(item, 1);
            }
        });
        holder.setOnClickListener(R.id.tvPayBalance, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pay(item, 2);
            }
        });
        holder.setOnClickListener(R.id.tvInfo, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goods.contains(item)) {
                    goods.remove(item);
                } else {
                    goods.add(item);
                }
                notifyDataSetChanged();
            }
        });
        holder.setChecked(R.id.cbStatus, goods.contains(item));
    }

    private void pay(final Goods item, final int type) {
        AlertDialog dialog = new AlertDialog.Builder(getContext()).create();
        dialog.setTitle(type == 1 ? getContext().getString(R.string.pay_cash) : getContext().getString(R.string.pay_balance));
        String message = "運單號: " + item.bar_code + "\n" +
                "重量: " + item.weight + "kg\n" +
                "尺寸: " + item.length + "cm*" + item.width + "cm*" + item.height + "cm\n" +
                "收費:￥" + item.price + "\n" +
                "用戶手機號碼：" + item.user_phone;
        dialog.setMessage(message);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getContext().getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                WebApi.payOrder(item.goods_id, type, new OnResponseListener<BaseResponse<String>>() {
                    @Override
                    public void onResponse(BaseResponse<String> response) {
                        if (response.getCode() == ApiCode.SUCCESS) {
                            ToastUtils.showShortToast(getContext(), R.string.pay_success);
                            goods.remove(item);
                            remove(item);
                        } else if (response.getCode() == 7) {
                            ToastUtils.showShortToast(getContext(), R.string.balance_enough);
                        } else {
                            ToastUtils.showShortToast(getContext(), R.string.pay_failed);
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        super.onError(error);
                        ToastUtils.showShortToast(getContext(), R.string.server_busy);
                    }
                });
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, getContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public void payAll(final int type) {
        if (goods.size() == 0) {
            ToastUtils.showShortToast(getContext(), R.string.no_select_good);
            return;
        }
        AlertDialog dialog = new AlertDialog.Builder(getContext()).create();
        dialog.setTitle(type == 1 ? getContext().getString(R.string.pay_cash) : getContext().getString(R.string.pay_balance));
        String ids = "";
        String codes = "";
        String user_phone = "";
        float priceF = 0;
        for (Goods item : goods) {
            ids += "," + item.goods_id;
            codes += ", " + item.bar_code;
            priceF += item.price;
            user_phone = item.user_phone;
        }
        ids = ids.replaceFirst(",", "");
        codes = codes.replaceFirst(",", "");
        String message = "運單號: " + codes +
                "\n收費:￥" + priceF + "\n" +
                "用戶手機號碼：" + user_phone;
        dialog.setMessage(message);
        final String finalIds = ids;
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getContext().getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                WebApi.payOrders(finalIds, type, new OnResponseListener<BaseResponse<String>>() {
                    @Override
                    public void onResponse(BaseResponse<String> response) {
                        if (response.getCode() == ApiCode.SUCCESS) {
                            ToastUtils.showShortToast(getContext(), R.string.pay_success);
                            removeAll(goods);
                            goods.clear();
                        } else if (response.getCode() == 7) {
                            ToastUtils.showShortToast(getContext(), R.string.balance_enough);
                        } else {
                            ToastUtils.showShortToast(getContext(), R.string.pay_failed);
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        super.onError(error);
                        ToastUtils.showShortToast(getContext(), R.string.server_busy);
                    }
                });
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, getContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    @Override
    public void notifyDataSetHasChanged() {
        super.notifyDataSetHasChanged();
        goods.clear();
        if (etScanCode != null) {
            etScanCode.getText().clear();
        }
    }
}
