package jimmy.com.charge.config;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public interface ApiUrl {

    String BASE_URL = "http://120.76.79.54:8887/";

    String LOGIN = BASE_URL + "api/payclient/login";
    String CHECK_USER = BASE_URL + "api/auth/getuserinfo";
    String GOODS_LIST = BASE_URL + "api/payclient/getorders";
    String PAY_ORDER = BASE_URL + "api/payclient/pay";
    String PAY_ORDERS = BASE_URL + "api/payclient/batchpay";

}
