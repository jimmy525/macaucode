package jimmy.com.charge.data;

/**
 * Created by Administrator on 2017/6/23.
 */

public class Goods {

    public String goods_id;
    public String bar_code;
    public String area;
    public String user_phone;
    public float length;
    public float width;
    public float height;
    public float weight;
    public String pay_way;
    public float price;
    public int status;
    public long update_time;

}
