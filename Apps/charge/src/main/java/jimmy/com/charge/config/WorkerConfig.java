package jimmy.com.charge.config;

import jimmy.com.charge.data.Worker;

/**
 * Created by Administrator on 2017/5/25.
 */

public class WorkerConfig {

    public static final String PAY = "1";

    private static WorkerConfig instance;

    public static synchronized WorkerConfig getInstance() {
        if (instance == null) {
            instance = new WorkerConfig();
        }
        return instance;
    }

    public Worker worker;

    public String getWorkerSign() {
        if (worker != null) {
            return worker.sign;
        } else {
            return "";
        }
    }

}
