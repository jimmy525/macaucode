package jimmy.com.charge;

import com.jimmy.common.base.app.BaseApplication;
import com.jimmy.common.util.HttpUtils;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jimmy on 2017/6/12 0012.
 */
public class ChargeApp extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        ZXingLibrary.initDisplayOpinion(this);
        initHttpDefaultHeader();
    }

    private void initHttpDefaultHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Basic MTIzNDU2OjEyMzQ1Ng==");
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("Accept", "application/json");
        header.put("Accept-Charset", "utf-8");
        HttpUtils.initDefaultHeader(header);
    }

}
