package jimmy.com.charge.data.binding;

import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.base.view.BaseDataBinding;
import com.jimmy.common.listener.OnResponseListener;

import java.util.List;

import jimmy.com.charge.api.WebApi;
import jimmy.com.charge.data.Goods;

/**
 * Created by Jimmy on 2017/6/15 0015.
 */
public class GoodsDataBinding implements BaseDataBinding<Goods> {

    private String phone;

    public GoodsDataBinding(String phone) {
        this.phone = phone;
    }

    @Override
    public void onStart(int page, int size, OnResponseListener<BaseResponse<List<Goods>>> listener) {
        loadBulletins(listener);
    }

    @Override
    public void onNext(int page, int size, OnResponseListener<BaseResponse<List<Goods>>> listener) {
        loadBulletins(listener);
    }

    private void loadBulletins(OnResponseListener<BaseResponse<List<Goods>>> listener) {
        WebApi.getOrdersList(phone, listener);
    }

}
