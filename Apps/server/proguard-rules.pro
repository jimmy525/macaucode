# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class jimmy.com.server.** {*;}
-keep class jimmy.com.server.activity.** {*;}
-keep class jimmy.com.server.api.** {*;}
-keep class jimmy.com.server.config.** {*;}
-keep class jimmy.com.server.data.** {*;}
-keep class jimmy.com.server.dialog.** {*;}
-keep class jimmy.com.server.fragment.** {*;}
