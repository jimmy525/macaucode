package jimmy.com.server.activity;

import android.view.View;

import com.google.gson.Gson;
import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.TemplateUtils;

import jimmy.com.server.R;
import jimmy.com.server.config.SharedKey;
import jimmy.com.server.config.WorkerConfig;
import jimmy.com.server.data.Worker;
import jimmy.com.server.fragment.AMLoginFragment;
import jimmy.com.server.fragment.AMMainFragment;
import jimmy.com.server.fragment.ZHLoginFragment;
import jimmy.com.server.fragment.ZHMainFragment;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_login);
        searchViewById(R.id.btnLoginFromZH).setOnClickListener(this);
        searchViewById(R.id.btnLoginFromAM).setOnClickListener(this);
    }

    @Override
    protected void initData() {
        super.initData();
        if (SharedUtils.getBoolean(this, SharedKey.ZH_IS_LOGIN)) {
            TemplateUtils.startTemplate(this, ZHMainFragment.class, getString(R.string.zh_warehouse));
            WorkerConfig.getInstance().worker = new Gson().fromJson(SharedUtils.getString(this, SharedKey.WORKER_CONFIG), Worker.class);
            finish();
        } else if (SharedUtils.getBoolean(this, SharedKey.AM_IS_LOGIN)){
            TemplateUtils.startTemplate(this, AMMainFragment.class, getString(R.string.am_warehouse));
            WorkerConfig.getInstance().worker = new Gson().fromJson(SharedUtils.getString(this, SharedKey.WORKER_CONFIG), Worker.class);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLoginFromZH:
                TemplateUtils.startTemplate(this, ZHLoginFragment.class, getString(R.string.zh_login));
                break;
            case R.id.btnLoginFromAM:
                TemplateUtils.startTemplate(this, AMLoginFragment.class, getString(R.string.am_login));
                break;
        }
    }
}
