package jimmy.com.server.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;

import com.jimmy.common.adapter.SuperAdapter;
import com.jimmy.common.adapter.SuperViewHolder;

import java.util.List;

import jimmy.com.server.R;
import jimmy.com.server.data.Store;

/**
 * Created by Jimmy on 2017/8/10 0010.
 */
public class SelectStoreAdapter extends SuperAdapter<Store> {

    public SelectStoreAdapter(Context context, List<Store> items, @LayoutRes int layoutResId) {
        super(context, items, layoutResId);
    }

    @Override
    public void onBind(SuperViewHolder holder, int viewType, int position, Store item) {
        holder.setText(R.id.tvName, item.store_name);
    }
}
