package jimmy.com.server.fragment;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.server.R;
import jimmy.com.server.activity.LoginActivity;
import jimmy.com.server.api.WebApi;
import jimmy.com.server.config.ApiCode;
import jimmy.com.server.config.SharedKey;
import jimmy.com.server.dialog.AMUploadDialog;

/**
 * Created by Jimmy on 2017/5/25 0025.
 */
public class AMMainFragment extends TemplateFragment implements View.OnClickListener {

    private EditText etScanCode, etDepot;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_am_main, container, false);
    }

    @Override
    protected void initView() {
        hideLeftButton();
        etScanCode = searchViewById(R.id.etScanCode);
        etDepot = searchViewById(R.id.etDepot);
        searchViewById(R.id.btnUpload).setOnClickListener(this);
        searchViewById(R.id.btnLogout).setOnClickListener(this);
        etScanCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return (event.getKeyCode() == KeyEvent.KEYCODE_ENTER);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpload:
                check();
                break;
            case R.id.btnLogout:
                logout();
                break;
        }
    }

    private void check() {
        final String code = etScanCode.getText().toString();
        final String depot = etDepot.getText().toString();
        if (TextUtils.isEmpty(code) || TextUtils.isEmpty(depot)) {
            ToastUtils.showShortToast(mActivity, R.string.am_info_is_empty);
        } else {
            AMUploadDialog dialog = new AMUploadDialog(mActivity);
            dialog.setData(code, depot);
            dialog.setOnConfirmListener(new AMUploadDialog.OnConfirmListener() {
                @Override
                public void onConfirm() {
                    upload(code, depot);
                }
            });
            dialog.show();
        }
    }

    private void upload(String code, String depot) {
        WebApi.uploadAM(code, depot, new OnResponseListener<BaseResponse<String>>() {
            @Override
            public void onResponse(BaseResponse<String> response) {
                if (response.getCode() == ApiCode.SUCCESS) {
                    ToastUtils.showShortToast(mActivity, R.string.am_upload_success);
                    clearText();
                } else {
                    ToastUtils.showShortToast(mActivity, R.string.am_upload_no_code_error);
                }
            }

            @Override
            public void onError(VolleyError error) {
                super.onError(error);
                ToastUtils.showShortToast(mActivity, R.string.am_upload_failed);
            }
        });
    }

    private void clearText() {
        etScanCode.getText().clear();
        etDepot.getText().clear();
        etScanCode.requestFocus();
    }

    private void logout() {
        SharedUtils.putBoolean(mActivity, SharedKey.AM_IS_LOGIN, false);
        Intent intent = new Intent(mActivity, LoginActivity.class);
        mActivity.startActivity(intent);
        mActivity.finish();
    }

}
