package jimmy.com.server.config;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public interface ApiUrl {

    String BASE_URL = "http://120.76.79.54:8887/";

    String ZH_LOGIN = BASE_URL + "api/auth/zhuhailogin";
    String AM_LOGIN = BASE_URL + "api/auth/macaulogin";
    String CHECK_USER = BASE_URL + "api/auth/getuserinfo";
    String ZH_UPLOAD = BASE_URL + "api/goods/zhuhaiupload";
    String AM_UPLOAD = BASE_URL + "api/goods/macauupload";

}
