package jimmy.com.server.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import jimmy.com.server.R;


/**
 * Created by Jimmy on 2017/5/25 0025.
 */
public class AMUploadDialog extends Dialog implements View.OnClickListener {

    private OnConfirmListener mOnConfirmListener;

    public AMUploadDialog(Context context) {
        super(context, R.style.DialogFullScreen);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        initView();
    }

    private void initView() {
        setContentView(R.layout.dialog_zh_upload);
        findViewById(R.id.btnCancel).setOnClickListener(this);
        findViewById(R.id.btnConfirm).setOnClickListener(this);
        findViewById(R.id.btnConfirmError).setVisibility(View.GONE);
    }

    public void setData(String code, String depot) {
        TextView tvData = (TextView) findViewById(R.id.tvData);
        Resources resources = getContext().getResources();
        String data = resources.getString(R.string.am_code) + ":" + code + "\n" +
                resources.getString(R.string.am_depot) + ":" + depot;
        tvData.setText(data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnConfirm:
                if (mOnConfirmListener != null) {
                    mOnConfirmListener.onConfirm();
                }
                dismiss();
                break;
        }
    }

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        mOnConfirmListener = onConfirmListener;
    }

    public interface OnConfirmListener {
        void onConfirm();
    }

}
