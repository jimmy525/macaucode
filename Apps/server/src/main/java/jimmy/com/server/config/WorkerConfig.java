package jimmy.com.server.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jimmy.com.server.data.Store;
import jimmy.com.server.data.Worker;

/**
 * Created by Administrator on 2017/5/25.
 */

public class WorkerConfig {

    public static final String ZHU_HAI = "2";
    public static final String AO_MEN = "3";

    private static WorkerConfig instance;

    public static synchronized WorkerConfig getInstance() {
        if (instance == null) {
            instance = new WorkerConfig();
        }
        return instance;
    }

    private Map<Integer, Store> stores = new HashMap<>();

    public Worker worker;

    public List<Store> getStores() {
        if (worker != null && worker.store != null && !worker.store.isEmpty())
            return worker.store;
        else
            return new ArrayList<>();
    }

    public String getWorkerSign() {
        if (worker != null) {
            return worker.sign;
        } else {
            return "";
        }
    }

    public Store getStore(int storeId) {
        if (worker != null && worker.store != null) {
            for (Store store : worker.store) {
                stores.put(store.store_id, store);
            }
        }
        return stores.get(storeId);
    }

}
