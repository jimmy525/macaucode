package jimmy.com.server.fragment;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.TemplateUtils;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.server.R;
import jimmy.com.server.api.WebApi;
import jimmy.com.server.config.ApiCode;
import jimmy.com.server.config.SharedKey;
import jimmy.com.server.config.WorkerConfig;
import jimmy.com.server.data.Worker;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public class ZHLoginFragment extends TemplateFragment implements View.OnClickListener {

    private EditText etUsername, etPassword;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_zh_login, container, false);
    }

    @Override
    protected void initView() {
        etUsername = searchViewById(R.id.etUsername);
        etPassword = searchViewById(R.id.etPassword);
        searchViewById(R.id.btnLogin).setOnClickListener(this);
    }

    @Override
    protected void initData() {
        super.initData();
        String username = SharedUtils.getString(mActivity, SharedKey.ZH_USERNAME);
        String password = SharedUtils.getString(mActivity, SharedKey.ZH_PASSWORD);
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            etUsername.setText(username);
            etPassword.setText(password);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                login();
                break;
        }
    }

    private void login() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            ToastUtils.showShortToast(mActivity, R.string.zh_login_info_is_empty);
            return;
        }
        SharedUtils.putString(mActivity, SharedKey.ZH_USERNAME, username);
        SharedUtils.putString(mActivity, SharedKey.ZH_PASSWORD, password);
        searchViewById(R.id.btnLogin).setEnabled(false);
        WebApi.loginZH(username, password, new OnResponseListener<BaseResponse<Worker>>() {
            @Override
            public void onResponse(BaseResponse<Worker> response) {
                if (response.getCode() == ApiCode.SUCCESS) {
                    if (response.getData() != null) {
                        ToastUtils.showShortToast(mActivity, R.string.zh_login_success);
                        WorkerConfig.getInstance().worker = response.getData();
                        SharedUtils.putBoolean(mActivity, SharedKey.ZH_IS_LOGIN, true);
                        SharedUtils.putString(mActivity, SharedKey.WORKER_CONFIG, new Gson().toJson(response.getData()));
                        TemplateUtils.startTemplateWithFlags(mActivity, ZHMainFragment.class, getAppString(R.string.zh_warehouse), Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    }
                } else if (response.getCode() == 2) {
                    ToastUtils.showShortToast(mActivity, R.string.zh_login_failed_no_user);
                } else {
                    ToastUtils.showShortToast(mActivity, R.string.zh_login_failed_password);
                }
                searchViewById(R.id.btnLogin).setEnabled(true);
            }

            @Override
            public void onError(VolleyError error) {
                super.onError(error);
                searchViewById(R.id.btnLogin).setEnabled(true);
            }
        });
    }
}
