package jimmy.com.server;

import com.jimmy.common.base.app.BaseApplication;
import com.jimmy.common.util.HttpUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/23.
 */

public class ServerApp extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        initHttpDefaultHeader();
    }

    private void initHttpDefaultHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Basic MTIzNDU2OjEyMzQ1Ng==");
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("Accept", "application/json");
        header.put("Accept-Charset", "utf-8");
        HttpUtils.initDefaultHeader(header);
    }
}
