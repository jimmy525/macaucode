package jimmy.com.server.api;

import com.google.gson.reflect.TypeToken;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.HttpUtils;

import java.util.HashMap;
import java.util.Map;

import jimmy.com.server.config.ApiUrl;
import jimmy.com.server.config.WorkerConfig;
import jimmy.com.server.data.User;
import jimmy.com.server.data.Worker;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public class WebApi {

    public static void loginZH(String username, String password, OnResponseListener<BaseResponse<Worker>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("admin_name", username);
        params.put("admin_password", password);
        params.put("admin_type", WorkerConfig.ZHU_HAI);
        HttpUtils.httpPost(ApiUrl.ZH_LOGIN, params, listener, new TypeToken<BaseResponse<Worker>>() {
        }.getType());
    }

    public static void loginAM(String username, String password, OnResponseListener<BaseResponse<Worker>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("admin_name", username);
        params.put("admin_password", password);
        params.put("admin_type", WorkerConfig.AO_MEN);
        HttpUtils.httpPost(ApiUrl.AM_LOGIN, params, listener, new TypeToken<BaseResponse<Worker>>() {
        }.getType());
    }


    public static void checkUser(String user_code, OnResponseListener<BaseResponse<User>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("user_phone", user_code);
        HttpUtils.httpPost(ApiUrl.CHECK_USER, params, listener, new TypeToken<BaseResponse<User>>() {
        }.getType());
    }

    public static void uploadZH(String code, String user_code, String length, String width, String height, String weight, String storeId, int isProblem, OnResponseListener<BaseResponse<String>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("bar_code", code);
        params.put("user_phone", user_code);
        params.put("sign", WorkerConfig.getInstance().getWorkerSign());
        params.put("length", String.valueOf(Math.max(Float.valueOf(length), 1)));
        params.put("width", String.valueOf(Math.max(Float.valueOf(width), 1)));
        params.put("height", String.valueOf(Math.max(Float.valueOf(height), 1)));
        params.put("weight", String.valueOf(Math.max(Float.valueOf(weight), 1)));
        params.put("store_id", storeId);
        params.put("is_problem", String.valueOf(isProblem));
        HttpUtils.httpPost(ApiUrl.ZH_UPLOAD, params, listener, new TypeToken<BaseResponse<String>>() {
        }.getType());
    }

    public static void uploadAM(String code, String area, OnResponseListener<BaseResponse<String>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("bar_code", code);
        params.put("area", area);
        params.put("sign", WorkerConfig.getInstance().getWorkerSign());
        HttpUtils.httpPost(ApiUrl.AM_UPLOAD, params, listener, new TypeToken<BaseResponse<String>>() {
        }.getType());
    }

}
