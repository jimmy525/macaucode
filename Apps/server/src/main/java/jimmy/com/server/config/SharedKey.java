package jimmy.com.server.config;

/**
 * Created by Administrator on 2017/5/24.
 */

public interface SharedKey {

    String ZH_IS_LOGIN = "zh.is.login";
    String AM_IS_LOGIN = "am.is.login";
    String ZH_USERNAME = "zh.username";
    String AM_USERNAME = "am.username";
    String ZH_PASSWORD = "zh.password";
    String AM_PASSWORD = "am.password";
    String WORKER_CONFIG = "worker.config";

}
