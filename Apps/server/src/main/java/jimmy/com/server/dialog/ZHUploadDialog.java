package jimmy.com.server.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import jimmy.com.server.R;
import jimmy.com.server.config.WorkerConfig;


/**
 * Created by Jimmy on 2017/5/25 0025.
 */
public class ZHUploadDialog extends Dialog implements View.OnClickListener {

    private OnConfirmListener mOnConfirmListener;

    public ZHUploadDialog(Context context) {
        super(context, R.style.DialogFullScreen);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        initView();
    }

    private void initView() {
        setContentView(R.layout.dialog_zh_upload);
        findViewById(R.id.btnCancel).setOnClickListener(this);
        findViewById(R.id.btnConfirm).setOnClickListener(this);
        findViewById(R.id.btnConfirmError).setOnClickListener(this);
    }

    public void setData(String code, String userId, String name, String length, String width, String height, String weight, String storeId) {
        TextView tvData = (TextView) findViewById(R.id.tvData);
        Resources resources = getContext().getResources();
        String data = resources.getString(R.string.zh_code) + ":" + code + "\n" +
                resources.getString(R.string.zh_user_id) + ":" + userId + "\n" +
                resources.getString(R.string.zh_name) + ":" + name + "\n" +
                resources.getString(R.string.zh_length) + ":" + length + "\n" +
                resources.getString(R.string.zh_width) + ":" + width + "\n" +
                resources.getString(R.string.zh_height) + ":" + height + "\n" +
                resources.getString(R.string.zh_weight) + ":" + weight + "\n" +
                resources.getString(R.string.zh_store_name) + ":" + WorkerConfig.getInstance().getStore(Integer.parseInt(storeId)).store_name;
        tvData.setText(data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnConfirm:
                if (mOnConfirmListener != null) {
                    mOnConfirmListener.onConfirm();
                }
                dismiss();
                break;
            case R.id.btnConfirmError:
                if (mOnConfirmListener != null) {
                    mOnConfirmListener.onConfirmError();
                }
                dismiss();
                break;
        }
    }

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        mOnConfirmListener = onConfirmListener;
    }

    public interface OnConfirmListener {
        void onConfirm();

        void onConfirmError();
    }

}
