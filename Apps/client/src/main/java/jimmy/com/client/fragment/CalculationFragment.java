package jimmy.com.client.fragment;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;

import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.client.R;
import jimmy.com.client.config.ApiUrl;

/**
 * Created by Jimmy on 2017/6/15 0015.
 */
public class CalculationFragment extends TemplateFragment implements View.OnClickListener {

    private WebView wvCalculation;

    private EditText etLength, etWidth, etHeight, etWeight;
    private TextView tvResult;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_calculation, container, false);
    }

    @Override
    protected void initView() {
        wvCalculation = searchViewById(R.id.wvCalculation);
        etLength = searchViewById(R.id.etLength);
        etWidth = searchViewById(R.id.etWidth);
        etHeight = searchViewById(R.id.etHeight);
        etWeight = searchViewById(R.id.etWeight);
        tvResult = searchViewById(R.id.tvResult);
        searchViewById(R.id.tvConfirm).setOnClickListener(this);
        initCalculationWebView();
    }

    private void initCalculationWebView() {
        wvCalculation = searchViewById(R.id.wvCalculation);
        WebSettings setting = wvCalculation.getSettings();
        setting.setJavaScriptEnabled(true);
        setting.setUseWideViewPort(true);
        setting.setLoadWithOverviewMode(true);
        setting.setJavaScriptCanOpenWindowsAutomatically(true);
        wvCalculation.requestFocusFromTouch();
        wvCalculation.loadUrl(ApiUrl.CALCULATION);
        wvCalculation.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                super.shouldOverrideUrlLoading(view, request);
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvConfirm:
                calculation();
                break;
        }
    }

    private void calculation() {
        String length = etLength.getText().toString();
        String width = etWidth.getText().toString();
        String height = etHeight.getText().toString();
        String weight = etWeight.getText().toString();
        if (TextUtils.isEmpty(length) || TextUtils.isEmpty(width) || TextUtils.isEmpty(height) || TextUtils.isEmpty(weight)) {
            ToastUtils.showShortToast(mContext, R.string.input_info_no_null);
        } else {
            int result = calculation(Float.valueOf(length), Float.valueOf(width), Float.valueOf(height), Float.valueOf(weight));
            tvResult.setText(String.format("%sMOP", String.valueOf(result)));
        }
    }

    private int calculation(float length, float width, float height, float weight) {
        double weightHeavy; // 体积重
        if (length * width * height / 6000 % 1 == 0) {
            weightHeavy = length * width * height / 6000;
        } else {
            weightHeavy = (int) (length * width * height / 6000) + 0.5;
        }
        double firstMoney; // 首重价钱
        double addedMoney; // 续重价钱
        double discountMoney = 0; // 优惠价钱
        addedMoney = (Math.max(weight, weightHeavy) - 1) * 2;
        if (length <= 50 && width <= 50 && height <= 50) {
            if (length <= 10 && width <= 10 && height <= 10) {
                firstMoney = 6;
            } else if (length <= 20 && width <= 20 && height <= 20) {
                firstMoney = 7;
            } else if (length <= 30 && width <= 30 && height <= 30) {
                firstMoney = 8;
            } else if (length <= 40 && width <= 40 && height <= 40) {
                firstMoney = 9;
            } else {
                firstMoney = 10;
            }
            if (weight < weightHeavy && length > 20 && width > 20 && height > 20 && (length > 25 || width > 25 || height > 25)) {
                discountMoney = weightHeavy - weight;
            }
        } else {
            float sum = length + width + height;
            if (sum <= 90) {
                firstMoney = 12;
            } else if (sum <= 120) {
                firstMoney = 13;
            } else if (sum <= 150) {
                firstMoney = 15;
            } else if (sum <= 180) {
                firstMoney = 17;
            } else if (sum <= 210) {
                firstMoney = 19;
            } else {
                firstMoney = 21;
            }
            if (weightHeavy < 3) {
                float min = length, max = length;
                min = Math.min(min, width);
                min = Math.min(min, height);
                max = Math.max(max, width);
                max = Math.max(max, height);
                if (min < max * 0.1) {
                    discountMoney = 2;
                } else if (min < max * 0.2) {
                    discountMoney = 1;
                }
            }
        }
        return (int) (firstMoney + addedMoney - discountMoney);
    }

}
