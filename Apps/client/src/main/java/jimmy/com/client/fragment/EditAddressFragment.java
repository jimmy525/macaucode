package jimmy.com.client.fragment;

import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.client.R;
import jimmy.com.client.config.ApiUrl;
import jimmy.com.client.config.UserConfig;
import jimmy.com.client.data.AMAddress;
import jimmy.com.client.data.Address;
import jimmy.com.client.data.ZHAddress;

/**
 * Created by Jimmy on 2017/6/16 0016.
 */
public class EditAddressFragment extends TemplateFragment implements View.OnClickListener {

    public static final String ADDRESS = "address";

    private TextView tvTitle, tvReceiver, tvReceiverPhone, tvReceiverArea, tvReceiverStreet, tvReceiverAddress;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_edit_address, container, false);
    }

    @Override
    protected void initView() {
        tvTitle = searchViewById(R.id.tvTitle);
        tvReceiver = searchViewById(R.id.tvReceiver);
        tvReceiverPhone = searchViewById(R.id.tvReceiverPhone);
        tvReceiverArea = searchViewById(R.id.tvReceiverArea);
        tvReceiverStreet = searchViewById(R.id.tvReceiverStreet);
        tvReceiverAddress = searchViewById(R.id.tvReceiverAddress);
        searchViewById(R.id.tvReceiverCopy).setOnClickListener(this);
        searchViewById(R.id.tvReceiverPhoneCopy).setOnClickListener(this);
        searchViewById(R.id.tvReceiverAreaCopy).setOnClickListener(this);
        searchViewById(R.id.tvReceiverStreetCopy).setOnClickListener(this);
        searchViewById(R.id.tvReceiverAddressCopy).setOnClickListener(this);
    }

    @Override
    protected void bindData() {
        super.bindData();
        Address address = (Address) getArguments().getSerializable(ADDRESS);
        assert address != null;
        if (address instanceof ZHAddress) {
            ZHAddress zhAddress = (ZHAddress) address;
            tvTitle.setText(mContext.getResources().getString(R.string.zh_address_edit, zhAddress.zhstore_name));
            tvReceiver.setText(zhAddress.zhstore_name + " " + UserConfig.getInstance().user.user_name);
            tvReceiverPhone.setText(zhAddress.zhstore_phone);
            tvReceiverArea.setText(zhAddress.zhstore_area);
            tvReceiverStreet.setText(zhAddress.zhstore_street);
            tvReceiverAddress.setText(String.format("%s(ID:%s)", zhAddress.zhstore_address, UserConfig.getInstance().user.user_phone));
        } else {
            AMAddress amAddress = (AMAddress) address;
            tvTitle.setText(mContext.getResources().getString(R.string.am_address_edit, amAddress.store_name));
            tvReceiver.setText(amAddress.store_name + " " + UserConfig.getInstance().user.user_name);
            tvReceiverPhone.setText(amAddress.store_phone);
            searchViewById(R.id.llReceiverArea).setVisibility(View.GONE);
            searchViewById(R.id.llReceiverStreet).setVisibility(View.GONE);
            tvReceiverAddress.setText(String.format("%s(ID:%s)", amAddress.store_address, UserConfig.getInstance().user.user_phone));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvReceiverCopy:
                copy(tvReceiver.getText().toString());
                break;
            case R.id.tvReceiverPhoneCopy:
                copy(tvReceiverPhone.getText().toString());
                break;
            case R.id.tvReceiverAreaCopy:
                copy(tvReceiverArea.getText().toString());
                break;
            case R.id.tvReceiverStreetCopy:
                copy(tvReceiverStreet.getText().toString());
                break;
            case R.id.tvReceiverAddressCopy:
                copy(tvReceiverAddress.getText().toString());
                break;
        }
    }

    private void copy(String content) {
        ClipboardManager cm = (ClipboardManager) mContext.getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
        cm.setText(content);
        ToastUtils.showShortToast(mContext, getAppString(R.string.copy_success));
    }

}
