package jimmy.com.client.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jimmy.common.adapter.OnItemClickListener;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.util.TemplateUtils;
import com.jimmy.common.view.pagerv.PageRecyclerView;

import java.util.ArrayList;
import java.util.List;

import jimmy.com.client.R;
import jimmy.com.client.adapter.BulletinAdapter;
import jimmy.com.client.data.Bulletin;
import jimmy.com.client.data.binding.BulletinDataBinding;
import jimmy.com.client.view.ScrollChildSwipeRefreshLayout;

/**
 * Created by Administrator on 2017/6/5.
 */

public class BulletinFragment extends TemplateFragment {

    private ScrollChildSwipeRefreshLayout srlRefresh;
    private PageRecyclerView rvBulletins;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_bulletin, container, false);
    }

    @Override
    protected void initView() {
        srlRefresh = searchViewById(R.id.srlRefresh);
        rvBulletins = searchViewById(R.id.rvBulletins);
        initRefresh();
        initBulletins();
    }

    @Override
    protected void initData() {
        rvBulletins.setDataBinding(new BulletinDataBinding());
    }

    private void initBulletins() {
        List<Bulletin> bulletins = new ArrayList<>();
        BulletinAdapter bulletinAdapter = new BulletinAdapter(mContext, bulletins, R.layout.list_item_bulletin);
        rvBulletins.setAdapter(bulletinAdapter);
        rvBulletins.setRefreshLayout(srlRefresh);
        bulletinAdapter.setOnItemClickListener(new OnItemClickListener<Bulletin>() {
            @Override
            public void onItemClick(Bulletin data, View itemView, int viewType, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(BulletinDetailFragment.BULLETIN, data);
                TemplateUtils.startTemplate(mActivity, BulletinDetailFragment.class, data.news_title, bundle);
            }
        });
    }

    private void initRefresh() {
        srlRefresh.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        srlRefresh.setScrollUpChild(rvBulletins);
    }

}
