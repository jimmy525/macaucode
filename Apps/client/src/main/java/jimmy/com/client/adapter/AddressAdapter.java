package jimmy.com.client.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.TextView;

import com.jimmy.common.adapter.SuperAdapter;
import com.jimmy.common.adapter.SuperViewHolder;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.TemplateUtils;

import java.util.List;

import jimmy.com.client.R;
import jimmy.com.client.activity.LoginActivity;
import jimmy.com.client.config.SharedKey;
import jimmy.com.client.data.AMAddress;
import jimmy.com.client.data.Address;
import jimmy.com.client.data.ZHAddress;
import jimmy.com.client.dialog.YesOrNoDialog;
import jimmy.com.client.fragment.AddressDetailFragment;
import jimmy.com.client.fragment.EditAddressFragment;

/**
 * Created by Jimmy on 2017/6/16 0016.
 */
public class AddressAdapter extends SuperAdapter<Address> {

    public AddressAdapter(Context context, List<Address> items, @LayoutRes int layoutResId) {
        super(context, items, layoutResId);
    }

    @Override
    public void onBind(SuperViewHolder holder, int viewType, int position, final Address item) {
        holder.findViewById(R.id.ivAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedUtils.getBoolean(getContext(), SharedKey.IS_LOGIN)) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(EditAddressFragment.ADDRESS, item);
                    if (item instanceof ZHAddress) {
                        TemplateUtils.startTemplate(getContext(), EditAddressFragment.class, ((ZHAddress) item).zhstore_name, bundle);
                    } else {
                        TemplateUtils.startTemplate(getContext(), EditAddressFragment.class, ((AMAddress) item).store_name, bundle);
                    }
                } else {
                    new YesOrNoDialog(getContext(), R.string.go_login_hint, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getContext().startActivity(new Intent(getContext(), LoginActivity.class));
                        }
                    }).show();
                }
            }
        });
        TextView tvName = holder.findViewById(R.id.tvName);
        if (item instanceof ZHAddress) {
            tvName.setText(((ZHAddress) item).zhstore_name);
        } else {
            tvName.setText(((AMAddress) item).store_name);
        }
    }
}
