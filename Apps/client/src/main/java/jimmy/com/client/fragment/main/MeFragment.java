package jimmy.com.client.fragment.main;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.TemplateUtils;

import cn.jpush.android.api.JPushInterface;
import jimmy.com.client.R;
import jimmy.com.client.activity.LoginActivity;
import jimmy.com.client.activity.RegisterActivity;
import jimmy.com.client.api.WebApi;
import jimmy.com.client.config.SharedKey;
import jimmy.com.client.config.UserConfig;
import jimmy.com.client.data.User;
import jimmy.com.client.dialog.YesOrNoDialog;
import jimmy.com.client.fragment.DisclaimerFragment;
import jimmy.com.client.fragment.PickUpRecordFragment;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class MeFragment extends TemplateFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout srlRefresh;
    private TextView tvName, tvNoLogin, tvMoney, tvLogin, tvRegister, tvLogout;

    private boolean isLogin;

    public static MeFragment newInstance() {
        return new MeFragment();
    }

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_main_me, container, false);
    }

    @Override
    protected void initView() {
        srlRefresh = searchViewById(R.id.srlRefresh);
        tvName = searchViewById(R.id.tvName);
        tvNoLogin = searchViewById(R.id.tvNoLogin);
        tvMoney = searchViewById(R.id.tvMoney);
        tvLogin = searchViewById(R.id.tvLogin);
        tvRegister = searchViewById(R.id.tvRegister);
        tvLogout = searchViewById(R.id.tvLogout);
        initRefresh();
    }

    private void initRefresh() {
        srlRefresh.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        srlRefresh.setOnRefreshListener(this);
    }

    @Override
    protected void initData() {
        super.initData();
        updateUserInfo();
    }

    @Override
    protected void bindData() {
        super.bindData();
        isLogin = SharedUtils.getBoolean(mActivity, SharedKey.IS_LOGIN);
        if (isLogin) {
            tvLogin.setVisibility(View.GONE);
            tvRegister.setVisibility(View.GONE);
            tvNoLogin.setVisibility(View.GONE);
            tvName.setVisibility(View.VISIBLE);
            tvMoney.setVisibility(View.VISIBLE);
            tvLogout.setVisibility(View.VISIBLE);
            tvName.setText(UserConfig.getInstance().user.user_name + "\n" + UserConfig.getInstance().user.user_phone);
            resetMoney();
            tvLogout.setOnClickListener(this);
        } else {
            tvLogin.setVisibility(View.VISIBLE);
            tvRegister.setVisibility(View.VISIBLE);
            tvNoLogin.setVisibility(View.VISIBLE);
            tvLogout.setVisibility(View.GONE);
            tvName.setVisibility(View.GONE);
            tvMoney.setVisibility(View.GONE);
            tvLogin.setOnClickListener(this);
            tvRegister.setOnClickListener(this);
        }
        searchViewById(R.id.tvPickUpRecord).setOnClickListener(this);
        searchViewById(R.id.tvDisclaimer).setOnClickListener(this);
        srlRefresh.setRefreshing(false);
    }

    private void updateUserInfo() {
        if (SharedUtils.getBoolean(mActivity, SharedKey.IS_LOGIN)) {
            WebApi.userInfo(new OnResponseListener<BaseResponse<User>>() {
                @Override
                public void onResponse(BaseResponse<User> response) {
                    UserConfig.getInstance().user = response.getData();
                    SharedUtils.putString(mContext, SharedKey.USER_INFO, new Gson().toJson(response.getData()));
                    bindData();
                }
            });
        }
    }

    public void resetMoney() {
        tvMoney.setText(getAppString(R.string.money_format, UserConfig.getInstance().user.money));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLogin:
                startActivity(new Intent(mActivity, LoginActivity.class));
                break;
            case R.id.tvRegister:
                startActivity(new Intent(mActivity, RegisterActivity.class));
                break;
            case R.id.tvPickUpRecord:
                if (isLogin) {
                    TemplateUtils.startTemplate(mContext, PickUpRecordFragment.class, getAppString(R.string.pick_up_record));
                } else {
                    new YesOrNoDialog(mContext, R.string.go_login_hint, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(mActivity, LoginActivity.class));
                        }
                    }).show();
                }
                break;
            case R.id.tvDisclaimer:
                TemplateUtils.startTemplate(mContext, DisclaimerFragment.class, getAppString(R.string.disclaimer));
                break;
            case R.id.tvLogout:
                logout();
                break;
        }
    }

    private void logout() {
        new YesOrNoDialog(mContext, R.string.logout_hint, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedUtils.putBoolean(mActivity, SharedKey.IS_LOGIN, false);
                mActivity.finish();
                startActivity(new Intent(mActivity, LoginActivity.class));
                JPushInterface.stopPush(mActivity);
            }
        }).show();
    }

    @Override
    public void onRefresh() {
        if (isLogin) {
            updateUserInfo();
        } else {
            srlRefresh.postDelayed(new Runnable() {
                @Override
                public void run() {
                    srlRefresh.setRefreshing(false);
                }
            }, 500);
        }
    }
}
