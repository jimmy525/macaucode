package jimmy.com.client.data;

/**
 * Created by Administrator on 2017/5/25.
 */

public class Goods {

    public String goods_id;
    public String bar_code;
    public String area;
    public String user_phone;
    public String store_name;
    public float length;
    public float width;
    public float height;
    public float weight;
    public String pay_way;
    public int price;
    public int status;
    public long update_time;

}
