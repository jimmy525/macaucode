package jimmy.com.client.fragment;

import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.util.DateUtils;

import jimmy.com.client.R;
import jimmy.com.client.data.Bulletin;

/**
 * Created by Administrator on 2017/6/5.
 */

public class BulletinDetailFragment extends TemplateFragment {

    public static final String BULLETIN = "bulletin";

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_bulletin_detail, container, false);
    }

    @Override
    protected void initView() {
        TextView tvTitle = searchViewById(R.id.tvTitle);
        TextView tvContent = searchViewById(R.id.tvContent);
        TextView tvTime = searchViewById(R.id.tvTime);
        Bulletin bulletin = (Bulletin) getArguments().getSerializable(BULLETIN);
        if (bulletin != null) {
            tvTitle.setText(bulletin.news_title);
            tvContent.setText(Html.fromHtml(bulletin.news_content));
            tvTime.setText(DateUtils.format(bulletin.date, "yyyy/MM/dd"));
        }
    }
}
