package jimmy.com.client.fragment.main;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jimmy.common.base.app.TemplateFragment;

import jimmy.com.client.R;
import jimmy.com.client.config.ApiUrl;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class InformationFragment extends TemplateFragment {

    private WebView wvInformation;

    public static InformationFragment newInstance() {
        return new InformationFragment();
    }

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_main_information, container, false);
    }

    @Override
    protected void initView() {
        wvInformation = searchViewById(R.id.wvInformation);
        WebSettings setting = wvInformation.getSettings();
        setting.setJavaScriptEnabled(true);
        setting.setUseWideViewPort(true);
        setting.setLoadWithOverviewMode(true);
        setting.setJavaScriptCanOpenWindowsAutomatically(true);
        wvInformation.requestFocusFromTouch();
        wvInformation.loadUrl(ApiUrl.INFORMATION);
        wvInformation.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                super.shouldOverrideUrlLoading(view, request);
                return false;
            }
        });
    }

    public void onBack() {
        wvInformation.goBack();
    }
}
