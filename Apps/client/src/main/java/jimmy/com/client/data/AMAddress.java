package jimmy.com.client.data;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/6/22.
 */

public class AMAddress extends Address implements Serializable {

    public String store_id;
    public String store_name;
    public String store_phone;
    public String store_work_time;
    public String store_address;
    public String store_logo;
    public String store_map;

}
