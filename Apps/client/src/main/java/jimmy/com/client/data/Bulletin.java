package jimmy.com.client.data;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/6/5.
 */

public class Bulletin implements Serializable {

    public String news_id;
    public String news_title;
    public String news_content;
    public String date;

}
