package jimmy.com.client.fragment.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.config.ApiCode;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.DateUtils;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.TemplateUtils;
import com.jimmy.common.view.pagerv.PageRecyclerView;

import java.util.ArrayList;
import java.util.List;

import jimmy.com.client.R;
import jimmy.com.client.adapter.GoodsAdapter;
import jimmy.com.client.api.WebApi;
import jimmy.com.client.config.SharedKey;
import jimmy.com.client.data.Bulletin;
import jimmy.com.client.data.Goods;
import jimmy.com.client.data.binding.GoodsDataBinding;
import jimmy.com.client.fragment.BulletinDetailFragment;
import jimmy.com.client.fragment.BulletinFragment;
import jimmy.com.client.view.ScrollChildSwipeRefreshLayout;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class IndexFragment extends TemplateFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private ScrollChildSwipeRefreshLayout srlRefresh;
    private PageRecyclerView rvAllGoods;
    private TextView tvBulletin1, tvBulletin2, tvBulletin3, tvEmpty;

    private List<Bulletin> topBulletins = new ArrayList<>();

    public static IndexFragment newInstance() {
        return new IndexFragment();
    }

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_main_index, container, false);
    }

    @Override
    protected void initView() {
        srlRefresh = searchViewById(R.id.srlRefresh);
        rvAllGoods = searchViewById(R.id.rvAllGoods);
        tvBulletin1 = searchViewById(R.id.tvBulletin1);
        tvBulletin2 = searchViewById(R.id.tvBulletin2);
        tvBulletin3 = searchViewById(R.id.tvBulletin3);
        tvEmpty = searchViewById(R.id.tvEmpty);
        searchViewById(R.id.tvMoreMessage).setOnClickListener(this);
        searchViewById(R.id.tvBulletin1).setOnClickListener(this);
        searchViewById(R.id.tvBulletin2).setOnClickListener(this);
        searchViewById(R.id.tvBulletin3).setOnClickListener(this);
        searchViewById(R.id.llTopBulletins).setVisibility(View.GONE);
        initGoodsList();
        initRefreshView();
    }

    @Override
    protected void initData() {
        httpGetBulletins();
        httpGetGoodsList();
    }

    private void httpGetGoodsList() {
        if (SharedUtils.getBoolean(mContext, SharedKey.IS_LOGIN, false)) {
            rvAllGoods.setDataBinding(new GoodsDataBinding(0));
        }
    }

    private void httpGetBulletins() {
        WebApi.getBulletinList(0, 3, new OnResponseListener<BaseResponse<List<Bulletin>>>() {
            @Override
            public void onResponse(BaseResponse<List<Bulletin>> response) {
                if (response.getCode() == ApiCode.SUCCESS && response.getData() != null) {
                    searchViewById(R.id.llTopBulletins).setVisibility(View.VISIBLE);
                    if (response.getData().size() >= 1) {
                        tvBulletin1.setVisibility(View.VISIBLE);
                        tvBulletin1.setText(DateUtils.format(response.getData().get(0).date) + "    " + response.getData().get(0).news_title);
                    } else {
                        tvBulletin1.setVisibility(View.GONE);
                    }
                    if (response.getData().size() >= 2) {
                        tvBulletin2.setVisibility(View.VISIBLE);
                        tvBulletin2.setText(DateUtils.format(response.getData().get(1).date) + "    " + response.getData().get(1).news_title);
                    } else {
                        tvBulletin2.setVisibility(View.GONE);
                    }
                    if (response.getData().size() >= 3) {
                        tvBulletin3.setVisibility(View.VISIBLE);
                        tvBulletin3.setText(DateUtils.format(response.getData().get(2).date) + "    " + response.getData().get(2).news_title);
                    } else {
                        tvBulletin3.setVisibility(View.GONE);
                    }
                    topBulletins = response.getData();
                } else {
                    searchViewById(R.id.llTopBulletins).setVisibility(View.GONE);
                }
                srlRefresh.setRefreshing(false);
            }

            @Override
            public void onError(VolleyError error) {
                super.onError(error);
                searchViewById(R.id.llTopBulletins).setVisibility(View.GONE);
                srlRefresh.setRefreshing(false);
            }
        });
    }

    private void initRefreshView() {
        srlRefresh.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        srlRefresh.setScrollUpChild(rvAllGoods);
        srlRefresh.setOnRefreshListener(this);
    }

    private void initGoodsList() {
        List<Goods> goodsOrders = new ArrayList<>();
        GoodsAdapter goodsAdapter = new GoodsAdapter(mContext, goodsOrders, R.layout.list_item_goods, true);
        rvAllGoods.setAdapter(goodsAdapter);
        rvAllGoods.setRefreshLayout(srlRefresh);
        rvAllGoods.setEmptyView(tvEmpty);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvMoreMessage:
                TemplateUtils.startTemplate(mActivity, BulletinFragment.class, getAppString(R.string.bulletin_message));
                break;
            case R.id.tvBulletin1:
                openBulletinDetail(topBulletins.get(0));
                break;
            case R.id.tvBulletin2:
                openBulletinDetail(topBulletins.get(1));
                break;
            case R.id.tvBulletin3:
                openBulletinDetail(topBulletins.get(2));
                break;
        }
    }

    private void openBulletinDetail(Bulletin bulletin) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(BulletinDetailFragment.BULLETIN, bulletin);
        TemplateUtils.startTemplate(mActivity, BulletinDetailFragment.class, bulletin.news_title, bundle);
    }

    @Override
    public void onRefresh() {
        initData();
    }
}
