package jimmy.com.client.activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.ToastUtils;

import cn.jpush.android.api.JPushInterface;
import jimmy.com.client.R;
import jimmy.com.client.api.WebApi;
import com.jimmy.common.config.ApiCode;
import jimmy.com.client.config.SharedKey;
import jimmy.com.client.config.UserConfig;
import jimmy.com.client.data.User;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    private EditText etPhoneNumber, etPassword;
    private TextView tvPhoneNumberError, tvPasswordError, tvConfirm;

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_login);
        etPhoneNumber = searchViewById(R.id.etPhoneNumber);
        etPassword = searchViewById(R.id.etPassword);
        tvPhoneNumberError = searchViewById(R.id.tvPhoneNumberError);
        tvPasswordError = searchViewById(R.id.tvPasswordError);
        tvConfirm = searchViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(this);
        searchViewById(R.id.tvForgetPassword).setOnClickListener(this);
        searchViewById(R.id.ivClose).setOnClickListener(this);
        etPhoneNumber.addTextChangedListener(this);
    }

    @Override
    protected void initData() {
        super.initData();
        String phone = SharedUtils.getString(this, SharedKey.PHONE);
        String password = SharedUtils.getString(this, SharedKey.PASSWORD);
        if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(password)) {
            etPhoneNumber.setText(phone);
            etPassword.setText(password);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvForgetPassword:
                startActivity(new Intent(this, UpdatePasswordActivity.class));
                break;
            case R.id.tvConfirm:
                login();
                break;
            case R.id.ivClose:
                finish();
                break;
        }
    }

    private void login() {
        String phoneNumber = etPhoneNumber.getText().toString();
        String password = etPassword.getText().toString();
        SharedUtils.putString(this, SharedKey.PHONE, phoneNumber);
        SharedUtils.putString(this, SharedKey.PASSWORD, password);
        if (TextUtils.isEmpty(password) || TextUtils.isEmpty(phoneNumber)) {
            ToastUtils.showShortToast(this, R.string.user_info_no_null);
        } else if (phoneNumber.length() < 8) {
            ToastUtils.showShortToast(this, R.string.user_info_length_six);
        } else {
            tvConfirm.setEnabled(false);
            WebApi.login(phoneNumber, password, new OnResponseListener<BaseResponse<User>>() {
                @Override
                public void onResponse(BaseResponse<User> response) {
                    if (response.getCode() == 3) {
                        tvPasswordError.setVisibility(View.VISIBLE);
                    } else if (response.getCode() == ApiCode.SUCCESS){
                        ToastUtils.showShortToast(LoginActivity.this, R.string.login_success);
                        SharedUtils.putBoolean(LoginActivity.this, SharedKey.IS_LOGIN, true);
                        SharedUtils.putString(LoginActivity.this, SharedKey.USER_INFO, new Gson().toJson(response.getData()));
                        UserConfig.getInstance().user = response.getData();
                        JPushInterface.resumePush(LoginActivity.this);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    } else {
                        tvPhoneNumberError.setVisibility(View.VISIBLE);
                    }
                    tvConfirm.setEnabled(true);
                }

                @Override
                public void onError(VolleyError error) {
                    super.onError(error);
                    tvConfirm.setEnabled(true);
                }
            });
        }
    }

    private void hideError() {
        tvPhoneNumberError.setVisibility(View.GONE);
        tvPasswordError.setVisibility(View.GONE);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        hideError();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
