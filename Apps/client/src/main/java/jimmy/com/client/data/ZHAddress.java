package jimmy.com.client.data;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/6/22.
 */

public class ZHAddress extends Address implements Serializable {

    public String zhstore_id;
    public String zhstore_name;
    public String zhstore_phone;
    public String zhstore_area;
    public String zhstore_street;
    public String zhstore_address;
    public String zhstore_logo;

}
