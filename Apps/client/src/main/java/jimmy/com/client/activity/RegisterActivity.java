package jimmy.com.client.activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.client.R;
import jimmy.com.client.api.WebApi;
import com.jimmy.common.config.ApiCode;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class RegisterActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    private EditText etUsername, etPhoneNumber, etPassword, etConfirmPassword;
    private TextView tvUsernameError, tvPhoneNumberError, tvPasswordError, tvConfirmPasswordError, tvConfirm;

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_register);
        searchViewById(R.id.ivClose).setOnClickListener(this);
        etUsername = searchViewById(R.id.etUsername);
        etPhoneNumber = searchViewById(R.id.etPhoneNumber);
        etPassword = searchViewById(R.id.etPassword);
        etConfirmPassword = searchViewById(R.id.etConfirmPassword);
        tvUsernameError = searchViewById(R.id.tvUsernameError);
        tvPhoneNumberError = searchViewById(R.id.tvPhoneNumberError);
        tvPasswordError = searchViewById(R.id.tvPasswordError);
        tvConfirmPasswordError = searchViewById(R.id.tvConfirmPasswordError);
        tvConfirm = searchViewById(R.id.tvConfirm);
        etUsername.addTextChangedListener(this);
        etPhoneNumber.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);
        etConfirmPassword.addTextChangedListener(this);
        tvConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.tvConfirm:
                register();
                break;
        }
    }

    private void register() {
        String username = etUsername.getText().toString();
        String phoneNumber = etPhoneNumber.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPassword)) {
            ToastUtils.showShortToast(this, R.string.input_info_no_null);
        } else if (!TextUtils.equals(password, confirmPassword)) {
            tvConfirmPasswordError.setVisibility(View.VISIBLE);
        } else if (username.length() > 10) {
            tvUsernameError.setText(View.VISIBLE);
        } else if (phoneNumber.charAt(0) != '6') {
            ToastUtils.showShortToast(RegisterActivity.this, R.string.phone_format_error);
        } else if (phoneNumber.length() != 8) {
            ToastUtils.showShortToast(RegisterActivity.this, R.string.phone_length_error);
        } else {
            tvConfirm.setEnabled(false);
            WebApi.register(username, phoneNumber, password, new OnResponseListener<BaseResponse<String>>() {
                @Override
                public void onResponse(BaseResponse<String> response) {
                    if (response.getCode() == 5) {
                        tvPhoneNumberError.setVisibility(View.VISIBLE);
                    } else if (response.getCode() == ApiCode.SUCCESS){
                        ToastUtils.showShortToast(RegisterActivity.this, R.string.register_success);
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    }
                    tvConfirm.setEnabled(true);
                }

                @Override
                public void onError(VolleyError error) {
                    super.onError(error);
                    tvConfirm.setEnabled(true);
                }
            });
        }
    }

    private void hideError() {
        tvUsernameError.setVisibility(View.GONE);
        tvPhoneNumberError.setVisibility(View.GONE);
        tvPasswordError.setVisibility(View.GONE);
        tvConfirmPasswordError.setVisibility(View.GONE);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        hideError();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
