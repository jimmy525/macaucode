package jimmy.com.client.view.seekbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import jimmy.com.client.R;

/**
 * Created by Jimmy on 2016/5/19 0019.
 */
public class SubsectionSeekBar extends LinearLayout {

    private final int DEFAULT_MAX = 100;
    private final int DEFAULT_PROGRESS = 0;
    private final int DEFAULT_DOTS = 4;
    private final boolean DEFAULT_IS_DISPLAY = false;

    private Context context;
    private SeekBar sbSubsection;
    private OnSeekBarChangeListener onSeekBarChangeListener;
    private int max;
    private int progress;
    private int dots;
    private int selectIndex;
    private int unProgressLineColor;
    private int unProgressCircleColor;
    private int progressColor;
    private boolean isDisplay; // 是否为展示

    public SubsectionSeekBar(Context context) {
        this(context, null);
    }

    public SubsectionSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SubsectionSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.SubsectionSeekBar);
        max = array.getInteger(R.styleable.SubsectionSeekBar_max, DEFAULT_MAX);
        progress = array.getInteger(R.styleable.SubsectionSeekBar_progress, DEFAULT_PROGRESS);
        dots = array.getInteger(R.styleable.SubsectionSeekBar_dots, DEFAULT_DOTS);
        unProgressLineColor = array.getColor(R.styleable.SubsectionSeekBar_unProgressLineColor, Color.parseColor("#E4E4E4"));
        unProgressCircleColor = array.getColor(R.styleable.SubsectionSeekBar_unProgressCircleColor, Color.parseColor("#E4E4E4"));
        progressColor = array.getColor(R.styleable.SubsectionSeekBar_progressColor, Color.parseColor("#FF5500"));
        isDisplay = array.getBoolean(R.styleable.SubsectionSeekBar_isDisplay, DEFAULT_IS_DISPLAY);
        array.recycle();
        if (progress > max) {
            progress = max;
        }
        if (dots > max) {
            throw new IllegalArgumentException("SubsectionSeekBar dots must be less than max");
        }
        if (max % (dots - 1) != 0) {
            throw new IllegalArgumentException("SubsectionSeekBar max must be divisible by (dots-1)");
        }
        selectIndex = progress / (max / (dots - 1));
        initView();
    }

    private void initView() {
        LinearLayout view = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.view_subsection_seekbar, this);
        sbSubsection = (SeekBar) view.findViewById(R.id.sbSeekBar);
        initSeekBar();
    }

    private void initSeekBar() {
        sbSubsection.setMax(max);
        sbSubsection.setProgress(progress);
        sbSubsection.setProgressDrawable(new SubsectionDrawable(sbSubsection, dots, unProgressLineColor, unProgressCircleColor, progressColor, selectIndex));
        if (!isDisplay) {
            sbSubsection.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (onSeekBarChangeListener != null) {
                        onSeekBarChangeListener.onProgressChanged(seekBar, progress, fromUser);
                    }
                    SubsectionSeekBar.this.progress = progress;
                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                    if (onSeekBarChangeListener != null) {
                        onSeekBarChangeListener.onStartTrackingTouch(seekBar);
                    }
                }

                public void onStopTrackingTouch(SeekBar seekBar) {
                    float subsectionValue = max / (dots - 1);
                    float halfOfSubsectionValue = subsectionValue / 2;
                    if (progress > max - halfOfSubsectionValue) {
                        seekBar.setProgress(max);
                        selectIndex = dots - 1;
                        if (onSeekBarChangeListener != null) {
                            onSeekBarChangeListener.onStopTrackingTouch(seekBar, selectIndex);
                        }
                        return;
                    }
                    if (progress < halfOfSubsectionValue) {
                        seekBar.setProgress(0);
                        selectIndex = 0;
                        if (onSeekBarChangeListener != null) {
                            onSeekBarChangeListener.onStopTrackingTouch(seekBar, selectIndex);
                        }
                        return;
                    }
                    float accumulatedValue = 0;
                    int recordingIndex = 0;
                    while (progress > accumulatedValue) {
                        accumulatedValue += subsectionValue;
                        recordingIndex++;
                    }
                    if (progress < accumulatedValue - halfOfSubsectionValue) {
                        seekBar.setProgress(Math.round(accumulatedValue - subsectionValue));
                        recordingIndex--;
                    } else {
                        seekBar.setProgress(Math.round(accumulatedValue));
                    }
                    selectIndex = recordingIndex;
                    if (onSeekBarChangeListener != null) {
                        onSeekBarChangeListener.onStopTrackingTouch(seekBar, selectIndex);
                    }
                }
            });
        } else {
            sbSubsection.setEnabled(false);
        }
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener onSeekBarChangeListener) {
        this.onSeekBarChangeListener = onSeekBarChangeListener;
    }

    public void setSelectedIndex(int index) {
        if (index >= dots) {
            throw new IllegalArgumentException("index must less than (dots-1)");
        }
        selectIndex = index;
        float subsectionValue = max / (dots - 1);
        float halfOfSubsectionValue = subsectionValue / 2;
        if (subsectionValue * index > max - halfOfSubsectionValue) {
            sbSubsection.setProgress(max);
        } else {
            sbSubsection.setProgress(Math.round(subsectionValue * index));
        }
        sbSubsection.setProgressDrawable(new SubsectionDrawable(sbSubsection, dots, unProgressLineColor, unProgressCircleColor, progressColor, selectIndex));
    }

    public int getSelectedIndex() {
        return selectIndex;
    }

    public interface OnSeekBarChangeListener {
        void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser);

        void onStartTrackingTouch(SeekBar seekBar);

        void onStopTrackingTouch(SeekBar seekBar, int selectIndex);
    }
}
