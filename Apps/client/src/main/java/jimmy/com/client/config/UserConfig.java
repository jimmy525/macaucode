package jimmy.com.client.config;

import jimmy.com.client.data.User;

/**
 * Created by Administrator on 2017/6/13.
 */

public class UserConfig {

    private static UserConfig instance;

    public static synchronized UserConfig getInstance() {
        if (instance == null) {
            instance = new UserConfig();
        }
        return instance;
    }

    public User user;

    public String getUserSign() {
        if (user != null) {
            return user.sign;
        } else {
            return "";
        }
    }

}
