package jimmy.com.client.fragment;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.config.ApiCode;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.DateUtils;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.client.R;
import jimmy.com.client.api.WebApi;
import jimmy.com.client.data.Goods;

/**
 * Created by Jimmy on 2018/3/9 0009.
 */
public class TaoBaoClaimFragment extends TemplateFragment implements View.OnClickListener {

    private EditText etSearchKeyWord;
    private LinearLayout llResult;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_tao_bao_claim, container, false);
    }

    @Override
    protected void initView() {
        etSearchKeyWord = searchViewById(R.id.etSearchKeyWord);
        llResult = searchViewById(R.id.llResult);
        searchViewById(R.id.tvSearch).setOnClickListener(this);
        initSearchList();
    }

    private void initSearchList() {
        llResult.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSearch:
                search();
                break;
        }
    }

    private void search() {
        String keyword = etSearchKeyWord.getText().toString();
        if (TextUtils.isEmpty(keyword)) {
            ToastUtils.showShortToast(getContext(), R.string.search_is_no_null);
            return;
        }
        WebApi.searchProblem(keyword, new OnResponseListener<BaseResponse<Goods>>() {
            @Override
            public void onResponse(BaseResponse<Goods> response) {
                if (response.getCode() == ApiCode.SUCCESS && response.getData() != null) {
                    Goods item = response.getData();
                    llResult.setVisibility(View.VISIBLE);
                    TextView tvInfo = searchViewById(R.id.tvInfo);
                    TextView tvExpireDate = searchViewById(R.id.tvExpireDate);
                    String content = "運單號: " + item.bar_code + "\n" +
                            "重量: " + item.weight + "kg\n" + "尺寸: " + item.length + "cm * " + item.width + "cm * " + item.height + " cm\n收費:￥"
                            + item.price + "\n" + "所在店鋪：" + item.store_name;
                    tvInfo.setText(content);
                    tvExpireDate.setText(DateUtils.format(item.update_time * 1000 + 4 * 24 * 3600 * 1000) + " 到期");
                } else {
                    llResult.setVisibility(View.GONE);
                }
            }
        });
    }
}
