package jimmy.com.client.fragment;

import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.config.ApiCode;
import com.jimmy.common.listener.OnResponseListener;

import java.util.Map;

import jimmy.com.client.R;
import jimmy.com.client.api.WebApi;

/**
 * Created by Jimmy on 2017/6/22 0022.
 */
public class DisclaimerFragment extends TemplateFragment {

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_disclaimer, container, false);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        super.initData();
        WebApi.getDisclaimer(new OnResponseListener<BaseResponse<Map<String, String>>>() {
            @Override
            public void onResponse(BaseResponse<Map<String, String>> response) {
                if (response.getCode() == ApiCode.SUCCESS && response.getData() != null) {
                    TextView tvDisclaimer = searchViewById(R.id.tvDisclaimer);
                    tvDisclaimer.setText(Html.fromHtml(response.getData().get("disclaimer")));
                }
            }
        });
    }
}
