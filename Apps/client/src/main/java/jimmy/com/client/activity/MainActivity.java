package jimmy.com.client.activity;

import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.IdRes;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.client.R;
import jimmy.com.client.adapter.MainPageAdapter;
import jimmy.com.client.config.SharedKey;
import jimmy.com.client.config.UserConfig;
import jimmy.com.client.dialog.QrCodeDialog;
import jimmy.com.client.dialog.YesOrNoDialog;
import jimmy.com.client.helper.JPushHelper;

public class MainActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private ImageView ivMainTitle, ivMainQrCode;
    private TextView tvMainTitle;
    private ViewPager vpMainContent;
    private RadioGroup rgMainBottom;
    private ImageButton ibMainBack;

    private MainPageAdapter mainPageAdapter;
    private long[] mNotes = new long[2];

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_main);
        ivMainTitle = searchViewById(R.id.ivMainTitle);
        ibMainBack = searchViewById(R.id.ibMainBack);
        tvMainTitle = searchViewById(R.id.tvMainTitle);
        vpMainContent = searchViewById(R.id.vpMainContent);
        rgMainBottom = searchViewById(R.id.rgMainBottom);
        ivMainQrCode = searchViewById(R.id.ivMainQrCode);
        ivMainQrCode.setOnClickListener(this);
        ibMainBack.setOnClickListener(this);
        initMainContent();
    }

    @Override
    protected void bindData() {
        super.bindData();
        bindJPushAlisa();
    }

    private void bindJPushAlisa() {
        if (SharedUtils.getBoolean(this, SharedKey.IS_LOGIN)) {
            JPushHelper.newInstance().bindJPushAlisa(UserConfig.getInstance().user.user_phone);
        }
    }

    private void initMainContent() {
        mainPageAdapter = new MainPageAdapter(getSupportFragmentManager());
        vpMainContent.setAdapter(mainPageAdapter);
        vpMainContent.setOffscreenPageLimit(4);
        rgMainBottom.setOnCheckedChangeListener(this);
        vpMainContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changePage(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivMainQrCode:
                if (SharedUtils.getBoolean(this, SharedKey.IS_LOGIN)) {
                    new QrCodeDialog(this).show();
                } else {
                    new YesOrNoDialog(this, R.string.go_login_hint, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        }
                    }).show();
                }
                break;
            case R.id.ibMainBack:
                if (vpMainContent.getCurrentItem() == 2) {
                    mainPageAdapter.onInformationBack();
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rbMainIndex:
                changePage(0, false);
                break;
            case R.id.rbMainCenter:
                changePage(1, false);
                break;
            case R.id.rbMainInformation:
                changePage(2, false);
                break;
            case R.id.rbMainMe:
                changePage(3, false);
                break;
        }
    }

    private void changePage(int index, boolean isScroll) {
        vpMainContent.setCurrentItem(index);
        ibMainBack.setVisibility(View.GONE);
        switch (index) {
            case 0:
                resetTitle(true, R.string.main_index);
                if (isScroll)
                    rgMainBottom.check(R.id.rbMainIndex);
                break;
            case 1:
                resetTitle(false, R.string.main_center);
                if (isScroll)
                    rgMainBottom.check(R.id.rbMainCenter);
                break;
            case 2:
                resetTitle(false, R.string.main_information);
                ibMainBack.setVisibility(View.VISIBLE);
                if (isScroll)
                    rgMainBottom.check(R.id.rbMainInformation);
                break;
            case 3:
                resetTitle(false, R.string.main_me);
                if (isScroll)
                    rgMainBottom.check(R.id.rbMainMe);
                break;
        }
    }

    private void resetTitle(boolean isIndex, int textId) {
        ivMainTitle.setVisibility(isIndex ? View.VISIBLE : View.GONE);
        tvMainTitle.setVisibility(isIndex ? View.GONE : View.VISIBLE);
        ivMainQrCode.setVisibility(isIndex ? View.VISIBLE : View.GONE);
        tvMainTitle.setText(textId);
    }

    @Override
    public void onBackPressed() {
        System.arraycopy(mNotes, 1, mNotes, 0, mNotes.length - 1);
        mNotes[mNotes.length - 1] = SystemClock.uptimeMillis();
        if (SystemClock.uptimeMillis() - mNotes[0] < 1000) {
            finish();
        } else {
            ToastUtils.showShortToast(this, R.string.exit_app_hint);
        }
    }
}
