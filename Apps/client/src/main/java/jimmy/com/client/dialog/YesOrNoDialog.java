package jimmy.com.client.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import jimmy.com.client.R;

/**
 * Created by Jimmy on 2017/6/10 0010.
 */
public class YesOrNoDialog extends Dialog implements View.OnClickListener {

    private final View.OnClickListener onCancelClickListener;
    private final View.OnClickListener onConfirmClickListener;

    public YesOrNoDialog(Context context, int titleId, View.OnClickListener onConfirmClickListener) {
        this(context, context.getResources().getString(titleId), null, onConfirmClickListener);
    }

    public YesOrNoDialog(Context context, int titleId, View.OnClickListener onCancelClickListener, View.OnClickListener onConfirmClickListener) {
        this(context, context.getResources().getString(titleId), onCancelClickListener, onConfirmClickListener);
    }

    public YesOrNoDialog(Context context, String title, View.OnClickListener onConfirmClickListener) {
        this(context, title, null, onConfirmClickListener);
    }

    public YesOrNoDialog(Context context, String title, View.OnClickListener onCancelClickListener, View.OnClickListener onConfirmClickListener) {
        super(context, R.style.DialogFullScreen);
        this.onCancelClickListener = onCancelClickListener;
        this.onConfirmClickListener = onConfirmClickListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        initView(title);
    }

    private void initView(String title) {
        setContentView(R.layout.dialog_yes_or_no);
        findViewById(R.id.btnCancel).setOnClickListener(this);
        findViewById(R.id.btnConfirm).setOnClickListener(this);
        ((TextView) findViewById(R.id.tvTitle)).setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                if (onCancelClickListener != null) {
                    onCancelClickListener.onClick(v);
                }
                dismiss();
                break;
            case R.id.btnConfirm:
                if (onConfirmClickListener != null) {
                    onConfirmClickListener.onClick(v);
                }
                dismiss();
                break;
        }
    }

}
