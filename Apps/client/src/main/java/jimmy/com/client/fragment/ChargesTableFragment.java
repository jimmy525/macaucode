package jimmy.com.client.fragment;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.config.ApiCode;
import com.jimmy.common.listener.OnResponseListener;

import java.util.Map;

import jimmy.com.client.R;
import jimmy.com.client.api.WebApi;
import jimmy.com.client.config.ApiUrl;

/**
 * Created by Jimmy on 2017/6/15 0015.
 */
public class ChargesTableFragment extends TemplateFragment {

    private LinearLayout llNormal;
    private ImageView ivDetail;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_charges_table, container, false);
    }

    @Override
    protected void initView() {
        llNormal = searchViewById(R.id.llNormal);
        ivDetail = searchViewById(R.id.ivDetail);
    }

    @Override
    protected void initData() {
        super.initData();
        WebApi.getExpressChangeImg(new OnResponseListener<BaseResponse<Map<String, String>>>() {
            @Override
            public void onResponse(BaseResponse<Map<String, String>> response) {
                if (response.getCode() == ApiCode.SUCCESS && response.getData() != null) {
                    if (response.getData().containsKey("img_url")) {
                        String url = ApiUrl.BASE_IMG_URL + response.getData().get("img_url");
                        Glide.with(mContext).load(url).into(ivDetail);
                        ivDetail.setVisibility(View.VISIBLE);
                        llNormal.setVisibility(View.GONE);
                    } else {
                        ivDetail.setVisibility(View.GONE);
                        llNormal.setVisibility(View.VISIBLE);
                    }
                } else {
                    ivDetail.setVisibility(View.GONE);
                    llNormal.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
