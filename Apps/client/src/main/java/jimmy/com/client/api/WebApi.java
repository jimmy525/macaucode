package jimmy.com.client.api;

import com.google.gson.reflect.TypeToken;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.HttpUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jimmy.com.client.config.ApiUrl;
import jimmy.com.client.config.UserConfig;
import jimmy.com.client.data.AMAddress;
import jimmy.com.client.data.Address;
import jimmy.com.client.data.Bulletin;
import jimmy.com.client.data.Goods;
import jimmy.com.client.data.User;
import jimmy.com.client.data.ZHAddress;

/**
 * Created by Administrator on 2017/6/5.
 */

public class WebApi {

    public static void login(String phone, String password, OnResponseListener<BaseResponse<User>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("user_phone", phone);
        params.put("user_password", password);
        HttpUtils.httpPost(ApiUrl.LOGIN, params, listener, new TypeToken<BaseResponse<User>>() {
        }.getType());
    }

    public static void register(String username, String phone, String password, OnResponseListener<BaseResponse<String>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("user_name", username);
        params.put("user_phone", phone);
        params.put("user_password", password);
        HttpUtils.httpPost(ApiUrl.REGISTER, params, listener, new TypeToken<BaseResponse<String>>() {
        }.getType());
    }

    public static void userInfo(OnResponseListener<BaseResponse<User>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("sign", UserConfig.getInstance().getUserSign());
        HttpUtils.httpPost(ApiUrl.USER_INFO, params, listener, new TypeToken<BaseResponse<User>>() {
        }.getType());
    }

    public static void sendAuthCode(String phone, OnResponseListener<BaseResponse> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("user_phone", phone);
        HttpUtils.httpPost(ApiUrl.SEND_CODE, params, listener, new TypeToken<BaseResponse<String>>() {
        }.getType());
    }

    public static void updatePassword(String phone, String code, String password, OnResponseListener<BaseResponse> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("user_phone", phone);
        params.put("user_password", password);
        params.put("code", code);
        HttpUtils.httpPost(ApiUrl.UPDATE_PASSWORD, params, listener, new TypeToken<BaseResponse<String>>() {
        }.getType());
    }

    public static void getGoodsList(int done, OnResponseListener<BaseResponse<List<Goods>>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("sign", UserConfig.getInstance().getUserSign());
        params.put("done", String.valueOf(done));
        HttpUtils.httpPost(ApiUrl.GOOD_LIST, params, listener, new TypeToken<BaseResponse<List<Goods>>>() {
        }.getType());
    }

    public static void getBulletinList(int page, int size, OnResponseListener<BaseResponse<List<Bulletin>>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("page", String.valueOf(page));
        params.put("size", String.valueOf(size));
        HttpUtils.httpPost(ApiUrl.BULLETIN_LIST, params, listener, new TypeToken<BaseResponse<List<Bulletin>>>() {
        }.getType());
    }

    public static void getTollAddressList(OnResponseListener<BaseResponse<List<Address>>> listener) {
        HttpUtils.httpPost(ApiUrl.ZH_STORES, new HashMap<String, String>(), listener, new TypeToken<BaseResponse<List<ZHAddress>>>() {
        }.getType());
    }

    public static void getShippingAddressList(OnResponseListener<BaseResponse<List<Address>>> listener) {
        HttpUtils.httpPost(ApiUrl.AM_STORES, new HashMap<String, String>(), listener, new TypeToken<BaseResponse<List<AMAddress>>>() {
        }.getType());
    }

    public static void getDisclaimer(OnResponseListener<BaseResponse<Map<String, String>>> listener) {
        HttpUtils.httpPost(ApiUrl.DISCLAIMER, new HashMap<String, String>(), listener, new TypeToken<BaseResponse<Map<String, String>>>() {
        }.getType());
    }

    public static void searchProblem(String keyword, OnResponseListener<BaseResponse<Goods>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("bar_code", keyword);
        HttpUtils.httpPost(ApiUrl.SEARCH_PROBLEM, params, listener, new TypeToken<BaseResponse<Goods>>() {
        }.getType());
    }

    public static void deleteOrder(String goodsId, OnResponseListener<BaseResponse<Object>> listener) {
        Map<String, String> params = new HashMap<>();
        params.put("goods_id", goodsId);
        params.put("sign", UserConfig.getInstance().getUserSign());
        HttpUtils.httpPost(ApiUrl.DELETE_DELIVERY, params, listener, new TypeToken<BaseResponse<Object>>() {
        }.getType());
    }

    public static void getExpressChangeImg(OnResponseListener<BaseResponse<Map<String, String>>> listener) {
        HttpUtils.httpPost(ApiUrl.EXPRESS_CHANGE_IMG, new HashMap<String, String>(), listener, new TypeToken<BaseResponse<Map<String, String>>>() {
        }.getType());
    }
}
