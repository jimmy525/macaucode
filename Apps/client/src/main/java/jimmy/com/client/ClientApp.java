package jimmy.com.client;

import android.content.Context;

import com.jimmy.common.base.app.BaseApplication;
import com.jimmy.common.util.HttpUtils;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;

import java.util.HashMap;
import java.util.Map;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by Administrator on 2017/5/23.
 */

public class ClientApp extends BaseApplication {

    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        ZXingLibrary.initDisplayOpinion(this);
        initHttpDefaultHeader();
        initJPush();
    }

    private void initJPush() {
        JPushInterface.setDebugMode(BuildConfig.DEBUG);
        JPushInterface.init(this);
    }

    private void initHttpDefaultHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Basic MTIzNDU2OjEyMzQ1Ng==");
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("Accept", "application/json");
        header.put("Accept-Charset", "utf-8");
        HttpUtils.initDefaultHeader(header);
    }

}
