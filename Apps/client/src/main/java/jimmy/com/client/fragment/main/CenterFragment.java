package jimmy.com.client.fragment.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.util.TemplateUtils;

import jimmy.com.client.R;
import jimmy.com.client.data.binding.AddressDataBinding;
import jimmy.com.client.fragment.AddressFragment;
import jimmy.com.client.fragment.CalculationFragment;
import jimmy.com.client.fragment.ChargesTableFragment;
import jimmy.com.client.fragment.TaoBaoClaimFragment;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class CenterFragment extends TemplateFragment implements View.OnClickListener {

    public static CenterFragment newInstance() {
        return new CenterFragment();
    }

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_main_center, container, false);
    }

    @Override
    protected void initView() {
        searchViewById(R.id.cvZhShippingAddress).setOnClickListener(this);
        searchViewById(R.id.cvAmShippingAddress).setOnClickListener(this);
        searchViewById(R.id.cvCalculator).setOnClickListener(this);
        searchViewById(R.id.cvChargesTable).setOnClickListener(this);
        searchViewById(R.id.cvTaoBaoClaim).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvZhShippingAddress: {
                Bundle bundle = new Bundle();
                bundle.putInt(AddressFragment.ADDRESS_TYPE, AddressDataBinding.TYPE_TOLL);
                TemplateUtils.startTemplate(mContext, AddressFragment.class, getAppString(R.string.zh_shipping_address), bundle);
            }
            break;
            case R.id.cvAmShippingAddress: {
                Bundle bundle = new Bundle();
                bundle.putInt(AddressFragment.ADDRESS_TYPE, AddressDataBinding.TYPE_SHIPPING);
                TemplateUtils.startTemplate(mContext, AddressFragment.class, getAppString(R.string.am_shipping_address), bundle);
            }
            break;
            case R.id.cvCalculator:
                TemplateUtils.startTemplate(mContext, CalculationFragment.class, getAppString(R.string.freight_calculator));
                break;
            case R.id.cvChargesTable:
                TemplateUtils.startTemplate(mContext, ChargesTableFragment.class, getAppString(R.string.freight_charges_table));
                break;
            case R.id.cvTaoBaoClaim:
                TemplateUtils.startTemplate(mContext, TaoBaoClaimFragment.class, getAppString(R.string.tao_bao_claim));
                break;

        }
    }
}
