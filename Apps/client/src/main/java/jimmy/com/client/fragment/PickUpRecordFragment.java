package jimmy.com.client.fragment;

import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.view.pagerv.PageRecyclerView;

import java.util.ArrayList;
import java.util.List;

import jimmy.com.client.R;
import jimmy.com.client.adapter.GoodsAdapter;
import jimmy.com.client.data.Goods;
import jimmy.com.client.data.binding.GoodsDataBinding;
import jimmy.com.client.view.ScrollChildSwipeRefreshLayout;

/**
 * Created by Jimmy on 2017/6/22 0022.
 */
public class PickUpRecordFragment extends TemplateFragment {

    private ScrollChildSwipeRefreshLayout srlRefresh;
    private PageRecyclerView rvPickUpRecord;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_pick_up_record, container, false);
    }

    @Override
    protected void initView() {
        srlRefresh = searchViewById(R.id.srlRefresh);
        rvPickUpRecord = searchViewById(R.id.rvPickUpRecord);
        initRefresh();
        initPickUpRecord();
    }

    @Override
    protected void initData() {
        rvPickUpRecord.setDataBinding(new GoodsDataBinding(1));
    }

    @Override
    public void reloadData() {
        initData();
    }

    private void initPickUpRecord() {
        List<Goods> goods = new ArrayList<>();
        GoodsAdapter goodsAdapter = new GoodsAdapter(mContext, goods, R.layout.list_item_goods, false);
        goodsAdapter.setFragment(this);
        rvPickUpRecord.setAdapter(goodsAdapter);
        rvPickUpRecord.setRefreshLayout(srlRefresh);
    }

    private void initRefresh() {
        srlRefresh.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        srlRefresh.setScrollUpChild(rvPickUpRecord);
    }
}
