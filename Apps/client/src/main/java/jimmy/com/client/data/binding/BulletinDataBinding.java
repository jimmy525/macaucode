package jimmy.com.client.data.binding;

import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.base.view.BaseDataBinding;
import com.jimmy.common.listener.OnResponseListener;

import java.util.List;

import jimmy.com.client.api.WebApi;
import jimmy.com.client.data.Bulletin;

/**
 * Created by Jimmy on 2017/6/15 0015.
 */
public class BulletinDataBinding implements BaseDataBinding<Bulletin> {

    @Override
    public void onStart(int page, int size, OnResponseListener<BaseResponse<List<Bulletin>>> listener) {
        loadBulletins(page, size, listener);
    }

    @Override
    public void onNext(int page, int size, OnResponseListener<BaseResponse<List<Bulletin>>> listener) {
        loadBulletins(page, size, listener);
    }

    private void loadBulletins(int page, int size, OnResponseListener<BaseResponse<List<Bulletin>>> listener) {
        WebApi.getBulletinList(page, size, listener);
    }

}
