package jimmy.com.client.data.binding;

import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.base.view.BaseDataBinding;
import com.jimmy.common.listener.OnResponseListener;

import java.util.List;

import jimmy.com.client.api.WebApi;
import jimmy.com.client.data.Goods;

/**
 * Created by Jimmy on 2017/6/15 0015.
 */
public class GoodsDataBinding implements BaseDataBinding<Goods> {

    private int done;

    public GoodsDataBinding(int done) {
        this.done = done;
    }

    @Override
    public void onStart(int page, int size, OnResponseListener<BaseResponse<List<Goods>>> listener) {
        loadGoods(listener);
    }

    @Override
    public void onNext(int page, int size, OnResponseListener<BaseResponse<List<Goods>>> listener) {
        loadGoods(listener);
    }

    private void loadGoods(OnResponseListener<BaseResponse<List<Goods>>> listener) {
        WebApi.getGoodsList(done, listener);
    }

}
