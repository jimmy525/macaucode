package jimmy.com.client.activity;

import android.content.Intent;

import com.google.gson.Gson;
import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.util.SharedUtils;

import jimmy.com.client.R;
import jimmy.com.client.config.SharedKey;
import jimmy.com.client.config.UserConfig;
import jimmy.com.client.data.User;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_splash);
        searchViewById(R.id.ivLogo).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedUtils.getBoolean(SplashActivity.this, SharedKey.IS_LOGIN)) {
                    UserConfig.getInstance().user = new Gson().fromJson(SharedUtils.getString(SplashActivity.this, SharedKey.USER_INFO), User.class);
                }
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 1500);
    }

}
