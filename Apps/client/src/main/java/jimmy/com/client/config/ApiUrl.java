package jimmy.com.client.config;

/**
 * Created by Jimmy on 2017/5/24 0024.
 */
public interface ApiUrl {

    String BASE_URL = "http://120.76.79.54:8887/";
    String BASE_IMG_URL = "http://120.76.79.54:8082/express/upload/";

    String INFORMATION = "http://www.macaoyi.com:8080";
    String REGISTER = BASE_URL + "api/app/register";
    String LOGIN = BASE_URL + "api/app/login";
    String DISCLAIMER = BASE_URL + "api/user/disclaimer";
    String SEND_CODE = BASE_URL + "api/app/getCode";
    String UPDATE_PASSWORD = BASE_URL + "api/app/forgetpassword";
    String USER_INFO = BASE_URL + "api/user/getuserinfo";
    String GOOD_LIST = BASE_URL + "api/user/delivery";
    String BULLETIN_LIST = BASE_URL + "api/app/news";
    String ZH_STORES = BASE_URL + "api/store/zhstores";
    String AM_STORES = BASE_URL + "api/store/stores";
    String SEARCH_PROBLEM = BASE_URL + "api/app/searchproblem";
    String DELETE_DELIVERY = BASE_URL + "api/user/deletedelivery";
    String EXPRESS_CHANGE_IMG = BASE_URL + "api/app/expresschangeimg";
    String CALCULATION = "http://www.macaoyi.com:8887/xg/index.html";

}
