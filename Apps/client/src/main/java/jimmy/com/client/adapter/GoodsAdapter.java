package jimmy.com.client.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jimmy.common.adapter.SuperAdapter;
import com.jimmy.common.adapter.SuperViewHolder;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.config.ApiCode;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.DateUtils;

import java.util.List;

import jimmy.com.client.R;
import jimmy.com.client.api.WebApi;
import jimmy.com.client.data.Goods;
import jimmy.com.client.dialog.YesOrNoDialog;
import jimmy.com.client.view.seekbar.SubsectionSeekBar;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class GoodsAdapter extends SuperAdapter<Goods> {

    private boolean isMain;
    private TemplateFragment fragment;

    public GoodsAdapter(Context context, List<Goods> items, @LayoutRes int layoutResId, boolean isMain) {
        super(context, items, layoutResId);
        this.isMain = isMain;
    }

    @Override
    public void onBind(SuperViewHolder holder, int viewType, int position, final Goods item) {
        SubsectionSeekBar sbProgress = holder.findViewById(R.id.sbProgress);
        sbProgress.setSelectedIndex(Math.min(item.status + 1, 3));
        TextView tvInfo = holder.findViewById(R.id.tvInfo);
        TextView tvStoreName = holder.findViewById(R.id.tvStoreName);
        String content = "運單號: " + item.bar_code + "\n" +
                "重量: " + item.weight + "kg，" + "尺寸: " + item.length + "cm*" + item.width + "cm*" + item.height + "cm 收費:￥" + item.price + "\n" +
                DateUtils.format(item.update_time * 1000 + 4 * 24 * 3600 * 1000) + "     到期";
        tvInfo.setText(content);
        tvStoreName.setText(item.store_name);
        ImageView ivDelete = holder.findViewById(R.id.ivDelete);
        ivDelete.setVisibility(isMain ? View.GONE : View.VISIBLE);
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new YesOrNoDialog(getContext(), R.string.is_delete_order, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteOrder(item.goods_id);
                    }
                }).show();
            }
        });
    }

    public void setFragment(TemplateFragment fragment) {
        this.fragment = fragment;
    }

    private void deleteOrder(String goodsId) {
        if (fragment == null)
            return;
        WebApi.deleteOrder(goodsId, new OnResponseListener<BaseResponse<Object>>() {
            @Override
            public void onResponse(BaseResponse<Object> response) {
                if (response.getCode() == ApiCode.SUCCESS) {
                    if (fragment != null)
                        fragment.reloadData();
                }
            }
        });
    }

}
