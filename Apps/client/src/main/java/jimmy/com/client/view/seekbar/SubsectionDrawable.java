package jimmy.com.client.view.seekbar;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.widget.SeekBar;

/**
 * Created by Jimmy on 2016/5/19 0019.
 */
public class SubsectionDrawable extends Drawable {

    private SeekBar seekBar;
    private int dots;
    private int unSelectLineColor;
    private int unSelectCircleColor;
    private int selectColor;
    private int selectIndex;
    private Paint linePaint;
    private Paint circlePaint;

    public SubsectionDrawable(SeekBar seekBar, int dots, int unSelectLineColor, int unSelectCircleColor, int selectColor, int selectIndex) {
        this.seekBar = seekBar;
        this.dots = dots;
        this.unSelectLineColor = unSelectLineColor;
        this.unSelectCircleColor = unSelectCircleColor;
        this.selectColor = selectColor;
        this.selectIndex = selectIndex;

        linePaint = new Paint();
        linePaint.setAntiAlias(true);
        linePaint.setStrokeWidth(toPix(5));

        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setStrokeWidth(toPix(5));
    }

    public void draw(@NonNull Canvas canvas) {
        float height = toPix(14) / 2;
        float width = getBounds().width() / (dots - 1);
        float radius = toPix(7);
        float x = 0;
        for (int i = 0; i < dots; i++) {
            if (i <= selectIndex) {
                linePaint.setColor(selectIndex == i ? unSelectLineColor : selectColor);
                circlePaint.setColor(selectColor);
                if (i != dots - 1)
                    canvas.drawLine(x, height, x + width, height, linePaint);
                canvas.drawCircle(x, height, radius, circlePaint);
            } else {
                linePaint.setColor(unSelectLineColor);
                circlePaint.setColor(unSelectCircleColor);
                if (i != dots - 1)
                    canvas.drawLine(x, height, x + width, height, linePaint);
                canvas.drawCircle(x, height, radius, circlePaint);
            }
            x += width;
        }
    }

    public void setAlpha(int alpha) {

    }

    public void setColorFilter(ColorFilter colorFilter) {

    }

    public int getOpacity() {
        return PixelFormat.TRANSPARENT;
    }

    private float toPix(int size) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, seekBar.getContext().getResources().getDisplayMetrics());
    }
}
