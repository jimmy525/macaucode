package jimmy.com.client.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jimmy.common.adapter.OnItemClickListener;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.TemplateUtils;
import com.jimmy.common.view.pagerv.PageRecyclerView;

import java.util.ArrayList;
import java.util.List;

import jimmy.com.client.R;
import jimmy.com.client.activity.LoginActivity;
import jimmy.com.client.adapter.AddressAdapter;
import jimmy.com.client.config.SharedKey;
import jimmy.com.client.data.AMAddress;
import jimmy.com.client.data.Address;
import jimmy.com.client.data.ZHAddress;
import jimmy.com.client.data.binding.AddressDataBinding;
import jimmy.com.client.dialog.YesOrNoDialog;

/**
 * Created by Jimmy on 2017/6/15 0015.
 */
public class AddressFragment extends TemplateFragment {

    public static final String ADDRESS_TYPE = "address.type";

    private SwipeRefreshLayout srlRefresh;
    private PageRecyclerView rvTollAddress;

    private int type;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_address, container, false);
    }

    @Override
    protected void initView() {
        srlRefresh = searchViewById(R.id.srlRefresh);
        rvTollAddress = searchViewById(R.id.rvAddress);
        initRefresh();
        initTollAddress();
    }

    @Override
    protected void initData() {
        type = getArguments().getInt(ADDRESS_TYPE, AddressDataBinding.TYPE_TOLL);
        rvTollAddress.setDataBinding(new AddressDataBinding(type));
    }

    private void initTollAddress() {
        List<Address> addresses = new ArrayList<>();
        AddressAdapter addressAdapter = new AddressAdapter(mContext, addresses, R.layout.list_item_address);
        rvTollAddress.setAdapter(addressAdapter);
        rvTollAddress.setRefreshLayout(srlRefresh);
        addressAdapter.setOnItemClickListener(new OnItemClickListener<Address>() {
            @Override
            public void onItemClick(Address data, View itemView, int viewType, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AddressDetailFragment.ADDRESS, data);
                if (data instanceof ZHAddress) {
                    if (SharedUtils.getBoolean(mContext, SharedKey.IS_LOGIN)) {
                        TemplateUtils.startTemplate(mContext, EditAddressFragment.class, ((ZHAddress) data).zhstore_name, bundle);
                    } else {
                        new YesOrNoDialog(mContext, R.string.go_login_hint, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mContext.startActivity(new Intent(mContext, LoginActivity.class));
                            }
                        }).show();
                    }
                } else {
                    TemplateUtils.startTemplate(mContext, AddressDetailFragment.class, ((AMAddress) data).store_name, bundle);
                }
            }
        });
    }

    private void initRefresh() {
        srlRefresh.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
    }

}
