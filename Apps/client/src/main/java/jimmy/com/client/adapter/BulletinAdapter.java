package jimmy.com.client.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.widget.TextView;

import com.jimmy.common.adapter.SuperAdapter;
import com.jimmy.common.adapter.SuperViewHolder;
import com.jimmy.common.util.DateUtils;

import java.util.List;

import jimmy.com.client.R;
import jimmy.com.client.data.Bulletin;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class BulletinAdapter extends SuperAdapter<Bulletin> {

    public BulletinAdapter(Context context, List<Bulletin> items, @LayoutRes int layoutResId) {
        super(context, items, layoutResId);
    }

    @Override
    public void onBind(SuperViewHolder holder, int viewType, int layoutPosition, Bulletin item) {
        TextView tvTitle = holder.findViewById(R.id.tvTitle);
        TextView tvTime = holder.findViewById(R.id.tvTime);
        tvTitle.setText(item.news_title);
        tvTime.setText(DateUtils.format(item.date, "yyyy/MM/dd"));
    }

}
