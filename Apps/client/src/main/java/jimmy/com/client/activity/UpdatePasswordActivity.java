package jimmy.com.client.activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.listener.OnResponseListener;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.ToastUtils;

import jimmy.com.client.R;
import jimmy.com.client.api.WebApi;

import com.jimmy.common.config.ApiCode;

import jimmy.com.client.config.SharedKey;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class UpdatePasswordActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    private EditText etPhoneNumber, etAuthCode, etPassword, etConfirmPassword;
    private TextView tvAuthCodeError, tvPasswordError, tvConfirmPasswordError, tvConfirm;

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_update_password);
        searchViewById(R.id.ivClose).setOnClickListener(this);
        searchViewById(R.id.tvGetAuthCode).setOnClickListener(this);
        tvConfirm = searchViewById(R.id.tvConfirm);
        etPhoneNumber = searchViewById(R.id.etPhoneNumber);
        etAuthCode = searchViewById(R.id.etAuthCode);
        etPassword = searchViewById(R.id.etPassword);
        etConfirmPassword = searchViewById(R.id.etConfirmPassword);
        tvAuthCodeError = searchViewById(R.id.tvAuthCodeError);
        tvPasswordError = searchViewById(R.id.tvPasswordError);
        tvConfirmPasswordError = searchViewById(R.id.tvConfirmPasswordError);
        etPhoneNumber.addTextChangedListener(this);
        etAuthCode.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);
        etConfirmPassword.addTextChangedListener(this);
        tvConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.tvGetAuthCode:
                sendAuthCode();
                break;
            case R.id.tvConfirm:
                updatePassword();
                break;
        }
    }

    private void updatePassword() {
        String phoneNumber = etPhoneNumber.getText().toString();
        String authCode = etAuthCode.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        if (TextUtils.isEmpty(authCode) || TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPassword)) {
            ToastUtils.showShortToast(this, R.string.input_info_no_null);
        } else if (!TextUtils.equals(password, confirmPassword)) {
            tvConfirmPasswordError.setVisibility(View.VISIBLE);
        } else {
            tvConfirm.setEnabled(false);
            WebApi.updatePassword(phoneNumber, authCode, password, new OnResponseListener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response.getCode() == ApiCode.SUCCESS) {
                        ToastUtils.showShortToast(UpdatePasswordActivity.this, R.string.update_password_success);
                        startActivity(new Intent(UpdatePasswordActivity.this, LoginActivity.class));
                        finish();
                    } else if (response.getCode() == 3) {
                        tvAuthCodeError.setVisibility(View.VISIBLE);
                    } else if (response.getCode() == 4) {
                        ToastUtils.showShortToast(UpdatePasswordActivity.this, R.string.auth_code_out_of_date);
                    } else if (response.getCode() == 5) {
                        ToastUtils.showShortToast(UpdatePasswordActivity.this, R.string.phone_number_is_no_exist);
                    } else {
                        ToastUtils.showShortToast(UpdatePasswordActivity.this, R.string.update_password_failed);
                    }
                    tvConfirm.setEnabled(true);
                }

                @Override
                public void onError(VolleyError error) {
                    super.onError(error);
                    tvConfirm.setEnabled(true);
                }
            });
        }
    }

    private void sendAuthCode() {
        String phone = etPhoneNumber.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShortToast(this, R.string.phone_is_no_null);
        } else if (System.currentTimeMillis() - SharedUtils.getLong(this, SharedKey.LAST_SEND_AUTH_CODE_TIME, 0) < 5 * 60 * 1000) {
            ToastUtils.showShortToast(this, R.string.dont_send_auth_code_quick);
        } else {
            WebApi.sendAuthCode(phone, new OnResponseListener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response.getCode() == ApiCode.SUCCESS) {
                        SharedUtils.putLong(UpdatePasswordActivity.this, SharedKey.LAST_SEND_AUTH_CODE_TIME, System.currentTimeMillis());
                        ToastUtils.showShortToast(UpdatePasswordActivity.this, R.string.send_auth_code_success);
                    } else if (response.getCode() == 1) {
                        ToastUtils.showShortToast(UpdatePasswordActivity.this, R.string.phone_number_is_no_exist);
                    } else {
                        ToastUtils.showShortToast(UpdatePasswordActivity.this, R.string.get_auth_code_failed);
                    }
                }
            });
        }
    }

    private void hideError() {
        tvAuthCodeError.setVisibility(View.INVISIBLE);
        tvPasswordError.setVisibility(View.GONE);
        tvConfirmPasswordError.setVisibility(View.GONE);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        hideError();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
