package jimmy.com.client.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import jimmy.com.client.fragment.main.CenterFragment;
import jimmy.com.client.fragment.main.IndexFragment;
import jimmy.com.client.fragment.main.InformationFragment;
import jimmy.com.client.fragment.main.MeFragment;

/**
 * Created by Jimmy on 2017/6/5 0005.
 */
public class MainPageAdapter extends FragmentPagerAdapter {

    private IndexFragment indexFragment;
    private CenterFragment centerFragment;
    private InformationFragment informationFragment;
    private MeFragment meFragment;

    public MainPageAdapter(FragmentManager fm) {
        super(fm);
        indexFragment = IndexFragment.newInstance();
        centerFragment = CenterFragment.newInstance();
        informationFragment = InformationFragment.newInstance();
        meFragment = MeFragment.newInstance();
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return indexFragment;
        } else if (position == 1) {
            return centerFragment;
        } else if (position == 2){
            return informationFragment;
        } else {
            return meFragment;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    public void onInformationBack() {
        informationFragment.onBack();
    }

}
