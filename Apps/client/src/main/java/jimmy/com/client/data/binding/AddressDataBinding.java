package jimmy.com.client.data.binding;

import com.jimmy.common.base.net.BaseResponse;
import com.jimmy.common.base.view.BaseDataBinding;
import com.jimmy.common.listener.OnResponseListener;

import java.util.List;

import jimmy.com.client.api.WebApi;
import jimmy.com.client.data.Address;

/**
 * Created by Jimmy on 2017/6/16 0016.
 */
public class AddressDataBinding implements BaseDataBinding<Address> {

    public static final int TYPE_TOLL = 0;
    public static final int TYPE_SHIPPING = 1;
    private int type;

    public AddressDataBinding(int type) {
        this.type = type;
    }

    @Override
    public void onStart(int page, int size, OnResponseListener<BaseResponse<List<Address>>> listener) {
        if (type == TYPE_TOLL)
            WebApi.getTollAddressList(listener);
        else
            WebApi.getShippingAddressList(listener);
    }

    @Override
    public void onNext(int page, int size, OnResponseListener<BaseResponse<List<Address>>> listener) {

    }

}
