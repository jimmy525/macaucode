package jimmy.com.client.config;

/**
 * Created by Administrator on 2017/5/24.
 */

public interface SharedKey {

    String IS_LOGIN = "is.login";
    String JPUSH_ALISA = "jpush.alisa";
    String PHONE = "phone";
    String PASSWORD = "password";
    String LAST_SEND_AUTH_CODE_TIME = "last.send.auth.code.time";
    String USER_INFO = "user.info";

}
