package jimmy.com.client.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.uuzuche.lib_zxing.activity.CodeUtils;

import jimmy.com.client.R;
import jimmy.com.client.config.UserConfig;

/**
 * Created by Jimmy on 2017/6/12 0012.
 */
public class QrCodeDialog extends Dialog {

    public QrCodeDialog(@NonNull Context context) {
        super(context, R.style.DialogFullScreen);
        initView();
    }

    private void initView() {
        setContentView(R.layout.dialog_qr_code);
        findViewById(R.id.flContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        ImageView ivQrCode = (ImageView) findViewById(R.id.ivQrCode);
        TextView tvName = (TextView) findViewById(R.id.tvName);
        String title = UserConfig.getInstance().user.user_name + "\n" + UserConfig.getInstance().user.user_phone;
        tvName.setText(title);
        int size = getContext().getResources().getDimensionPixelOffset(R.dimen.qr_code_size);
        ivQrCode.setImageBitmap(CodeUtils.createImage(UserConfig.getInstance().user.user_phone, size, size, null));
    }

}
