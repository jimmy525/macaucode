package jimmy.com.client.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jimmy.common.base.app.TemplateFragment;
import com.jimmy.common.util.SharedUtils;
import com.jimmy.common.util.TemplateUtils;

import jimmy.com.client.R;
import jimmy.com.client.activity.LoginActivity;
import jimmy.com.client.config.ApiUrl;
import jimmy.com.client.config.SharedKey;
import jimmy.com.client.data.AMAddress;
import jimmy.com.client.data.Address;
import jimmy.com.client.data.ZHAddress;
import jimmy.com.client.dialog.YesOrNoDialog;

/**
 * Created by Jimmy on 2017/6/16 0016.
 */
public class AddressDetailFragment extends TemplateFragment implements View.OnClickListener {

    public static final String ADDRESS = "address";

    private TextView tvName, tvPhone, tvTime, tvPassword, tvToll;
    private ImageView ivIcon, ivMap;

    private Address address;

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_address_detail, container, false);
    }

    @Override
    protected void initView() {
        tvName = searchViewById(R.id.tvName);
        tvPhone = searchViewById(R.id.tvPhone);
        tvTime = searchViewById(R.id.tvTime);
        tvPassword = searchViewById(R.id.tvPassword);
        tvToll = searchViewById(R.id.tvToll);
        ivIcon = searchViewById(R.id.ivIcon);
        ivMap = searchViewById(R.id.ivMap);
        ivIcon.setOnClickListener(this);
    }

    @Override
    protected void bindData() {
        super.bindData();
        address = (Address) getArguments().getSerializable(ADDRESS);
        assert address != null;
        if (address instanceof ZHAddress) {
            ZHAddress zhAddress = (ZHAddress) address;
            tvName.setText(zhAddress.zhstore_name);
            tvPhone.setText(getAppString(R.string.store_phone) + zhAddress.zhstore_phone);
            tvTime.setVisibility(View.GONE);
            tvPassword.setVisibility(View.GONE);
            tvToll.setVisibility(View.GONE);
            Glide.with(mContext).load(ApiUrl.BASE_IMG_URL + zhAddress.zhstore_logo).into(ivIcon);
            ivMap.setVisibility(View.GONE);
        } else {
            AMAddress amAddress = (AMAddress) address;
            tvName.setText(amAddress.store_name);
            tvPhone.setText(getAppString(R.string.store_phone) + amAddress.store_phone);
            tvTime.setText(getAppString(R.string.on_time) + amAddress.store_work_time);
            tvToll.setText(getAppString(R.string.pick_good_address) + amAddress.store_address);
            tvPassword.setVisibility(View.GONE);
            Glide.with(mContext).load(ApiUrl.BASE_IMG_URL + amAddress.store_logo).into(ivIcon);
            Glide.with(mContext).load(ApiUrl.BASE_IMG_URL + amAddress.store_map).into(ivMap);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivIcon:
                editAddress();
                break;
        }
    }

    private void editAddress() {
        if (SharedUtils.getBoolean(mContext, SharedKey.IS_LOGIN)) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(EditAddressFragment.ADDRESS, address);
            if (address instanceof ZHAddress) {
                TemplateUtils.startTemplate(mContext, EditAddressFragment.class, ((ZHAddress) address).zhstore_name, bundle);
            } else {
                TemplateUtils.startTemplate(mContext, EditAddressFragment.class, ((AMAddress) address).store_name, bundle);
            }
        } else {
            new YesOrNoDialog(mContext, R.string.go_login_hint, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                }
            }).show();
        }
    }
}
