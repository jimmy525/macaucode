package jimmy.com.cinema;

import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jimmy.common.base.app.BaseActivity;

public class MainActivity extends BaseActivity {

    private WebView wvCinema;

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_main);
        wvCinema = searchViewById(R.id.wvCinema);
        WebSettings setting = wvCinema.getSettings();
        setting.setJavaScriptEnabled(true);
        setting.setUseWideViewPort(true);
        setting.setLoadWithOverviewMode(true);
        setting.setJavaScriptCanOpenWindowsAutomatically(true);
        wvCinema.requestFocusFromTouch();
        wvCinema.loadUrl(ApiUrl.CINEMA_URL);
        wvCinema.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                super.shouldOverrideUrlLoading(view, request);
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (wvCinema.canGoBack()) {
            wvCinema.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
