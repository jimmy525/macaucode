package com.jimmy.common.util;

import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Jimmy on 2017/6/22 0022.
 */
public class DateUtils {

    public static String format(String date) {
        return format(date, null, null);
    }

    public static String format(String date, String toFormat) {
        return format(date, null, toFormat);
    }

    public static String format(String date, String formFormat, String toFormat) {
        try {
            if (TextUtils.isEmpty(formFormat)) {
                formFormat = "yyyy-MM-dd HH:mm:ss";
            }
            if (TextUtils.isEmpty(toFormat)) {
                toFormat = "MM-dd";
            }
            SimpleDateFormat from = new SimpleDateFormat(formFormat, Locale.CHINA);
            SimpleDateFormat to = new SimpleDateFormat(toFormat, Locale.CHINA);
            return to.format(from.parse(date).getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String format(long time) {
        return format(time, null);
    }

    public static String format(long time, String format) {
        try {
            if (TextUtils.isEmpty(format)) {
                format = "yyyy/MM/dd";
            }
            return new SimpleDateFormat(format, Locale.CHINA).format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}
