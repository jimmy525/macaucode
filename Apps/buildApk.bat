@echo off

call :buildGoogle %1
call :buildBaidu %1
call :buildWandoujia %1
call :buildSogou %1
call :buildQihoo360 %1
call :buildMeizu %1
call :buildHuawei %1
call :buildXiaomi %1
call :buildAnzhi %1
call :buildOppo %1
call :buildLenovo %1
call :buildServer %1
call :buildCharge %1
call :buildCinema %1

:buildGoogle
cd client
@rem %1
if "%1" == "all" echo -----------------------start build google release-----------------------
if "%1" == "google" echo -----------------------start build google release-----------------------
if "%1" == "all" call gradlew.bat assembleGoogleRelease
if "%1" == "google" call gradlew.bat assembleGoogleRelease
if "%1" == "all" echo ------------------------end build google release------------------------
if "%1" == "google" echo ------------------------end build google release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildBaidu
cd client
@rem %1
if "%1" == "all" echo -----------------------start build baidu release-----------------------
if "%1" == "baidu" echo -----------------------start build baidu release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaBaiduRelease
if "%1" == "baidu" call gradlew.bat assembleChinaBaiduRelease
if "%1" == "all" echo ------------------------end build baidu release------------------------
if "%1" == "baidu" echo ------------------------end build baidu release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildWandoujia
cd client
@rem %1
if "%1" == "all" echo -----------------------start build wandoujia release-----------------------
if "%1" == "wangoujia" echo -----------------------start build wandoujia release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaWandoujiaRelease
if "%1" == "wandoujia" call gradlew.bat assembleChinaWandoujiaRelease
if "%1" == "all" echo ------------------------end build wandoujia release------------------------
if "%1" == "wandoujia" echo ------------------------end build wandoujia release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildSogou
cd client
@rem %1
if "%1" == "all" echo -----------------------start build sogou release-----------------------
if "%1" == "sogou" echo -----------------------start build sogou release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaSogouRelease
if "%1" == "sogou" call gradlew.bat assembleChinaSogouRelease
if "%1" == "all" echo ------------------------end build sogou release------------------------
if "%1" == "sogou" echo ------------------------end build sogou release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildQihoo360
cd client
@rem %1
if "%1" == "all" echo -----------------------start build qihoo360 release-----------------------
if "%1" == "qihoo360" echo -----------------------start build qihoo360 release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaQihoo360Release
if "%1" == "qihoo360" call gradlew.bat assembleChinaQihoo360Release
if "%1" == "all" echo ------------------------end build qihoo360 release------------------------
if "%1" == "qihoo360" echo ------------------------end build qihoo360 release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildMeizu
cd client
@rem %1
if "%1" == "all" echo -----------------------start build meizu release-----------------------
if "%1" == "meizu" echo -----------------------start build meizu release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaMeizuRelease
if "%1" == "meizu" call gradlew.bat assembleChinaMeizuRelease
if "%1" == "all" echo ------------------------end build meizu release------------------------
if "%1" == "meizu" echo ------------------------end build meizu release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildHuawei
cd client
@rem %1
if "%1" == "all" echo -----------------------start build huawei release-----------------------
if "%1" == "huawei" echo -----------------------start build huawei release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaHuaweiRelease
if "%1" == "huawei" call gradlew.bat assembleChinaHuaweiRelease
if "%1" == "all" echo ------------------------end build huawei release------------------------
if "%1" == "huawei" echo ------------------------end build huawei release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildXiaomi
cd client
@rem %1
if "%1" == "all" echo -----------------------start build xiaomi release-----------------------
if "%1" == "xiaomi" echo -----------------------start build xiaomi release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaXiaomiRelease
if "%1" == "xiaomi" call gradlew.bat assembleChinaXiaomiRelease
if "%1" == "all" echo ------------------------end build xiaomi release------------------------
if "%1" == "xiaomi" echo ------------------------end build xiaomi release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildAnzhi
cd client
@rem %1
if "%1" == "all" echo -----------------------start build anzhi release-----------------------
if "%1" == "anzhi" echo -----------------------start build anzhi release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaAnzhiRelease
if "%1" == "anzhi" call gradlew.bat assembleChinaAnzhiRelease
if "%1" == "all" echo ------------------------end build anzhi release------------------------
if "%1" == "anzhi" echo ------------------------end build anzhi release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildOppo
cd client
@rem %1
if "%1" == "all" echo -----------------------start build oppo release-----------------------
if "%1" == "oppo" echo -----------------------start build oppo release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaOppoRelease
if "%1" == "oppo" call gradlew.bat assembleChinaOppoRelease
if "%1" == "all" echo ------------------------end build oppo release------------------------
if "%1" == "oppo" echo ------------------------end build oppo release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildLenovo
cd client
@rem %1
if "%1" == "all" echo -----------------------start build lenovo release-----------------------
if "%1" == "lenovo" echo -----------------------start build lenovo release-----------------------
if "%1" == "all" call gradlew.bat assembleChinaLenovoRelease
if "%1" == "lenovo" call gradlew.bat assembleChinaLenovoRelease
if "%1" == "all" echo ------------------------end build lenovo release------------------------
if "%1" == "lenovo" echo ------------------------end build lenovo release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildServer
cd server
@rem %1
if "%1" == "all" echo -----------------------start build server release-----------------------
if "%1" == "server" echo -----------------------start build server release-----------------------
if "%1" == "all" call gradlew.bat assembleGoogleRelease
if "%1" == "server" call gradlew.bat assembleGoogleRelease
if "%1" == "all" echo ------------------------end build server release------------------------
if "%1" == "server" echo ------------------------end build server release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildCharge
cd charge
@rem %1
if "%1" == "all" echo -----------------------start build charge release-----------------------
if "%1" == "charge" echo -----------------------start build charge release-----------------------
if "%1" == "all" call gradlew.bat assembleGoogleRelease
if "%1" == "charge" call gradlew.bat assembleGoogleRelease
if "%1" == "all" echo ------------------------end build charge release------------------------
if "%1" == "charge" echo ------------------------end build charge release------------------------
if "%1" == "all" echo ------------------------build next------------------------
goto end

:buildCinema
cd cinema
@rem %1
if "%1" == "all" echo -----------------------start build cinema release-----------------------
if "%1" == "cinema" echo -----------------------start build cinema release-----------------------
if "%1" == "all" call gradlew.bat assembleGoogleRelease
if "%1" == "cinema" call gradlew.bat assembleGoogleRelease
if "%1" == "all" echo ------------------------end build cinema release------------------------
if "%1" == "cinema" echo ------------------------end build cinema release------------------------
goto end

:end
cd ..